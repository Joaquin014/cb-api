<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class TicketsHelpType extends Model {
    
    protected $table = 'tickets_help_type';
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UnitsOperators extends Model
{
    protected $table = 'operators_units';
}

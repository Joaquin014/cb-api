<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IntranetSection extends Model
{
    //
    protected $table = 'intranet_section';
}

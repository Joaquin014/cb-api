<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientInvoicing extends Model
{
    //
    protected $table = 'clients_invoicing';
}

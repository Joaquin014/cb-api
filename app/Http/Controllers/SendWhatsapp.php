<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\TicketsNotifications;
use Carbon\Carbon;
use DB;

class SendWhatsapp extends Controller {
    public function get(){
        try {
          $data = TicketsNotifications::select('*')->where('send', '=', 'POR ENVIAR')->get();

          return response()->json($data);
        } catch (\Exception $e) {
          return response()->json($e);
        }
    }

    public function store(Request $request){
        try {
          $sms = new TicketsNotifications;
          $sms->id_ticket = $request->input('idTicket');
          $sms->from = $request->input('from');
          $sms->to = $request->input('to');
          $sms->message = $request->input('message');
          $sms->save();

          return response()->json($sms);
        } catch (\Exception $e) {
          return response()->json($e);
        }
    }

    public function update(Request $request){
        try {
            $data = TicketsNotifications::findOrFail($request->input('idNotification'));
            $data->send = 'ENVIADO';
            $data->save();

            return response()->json($data);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json(['error' => 'No se encontro la solicitud, asegurese de que el id enviado sea correcto.'], 404);
        } catch (\Exception $e) {
            return response()->json($e);
        }
    }
}

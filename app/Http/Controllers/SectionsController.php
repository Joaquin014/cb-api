<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\IntranetSection;
use DB;

class SectionsController extends Controller
{
    public function get(){
      try { 
          $data = IntranetSection::
                    join('intranet_department', 'intranet_department.id', '=', 'intranet_section.id_department')
                    ->select(
                      'intranet_section.id',
                      'intranet_section.id_department',
                      'intranet_department.name as name_department',
                      'intranet_section.name',
                      'intranet_section.description')
                    ->where('intranet_section.status', '=', '1')
                    ->orderBy('intranet_section.name', 'asc')
                    ->get();
                  
          return response()->json($data);

      } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
          return response()->json(['error' => 'No se encontraron resultados de la consulta.'], 404);
      } catch (\Exception $e) {
          return response()->json($e);
      }
    }

    public function store(Request $request){
        try {
            $data = new IntranetSection;
            $data->id_department = $request->input('idDepartment');
            $data->name = $request->input('name');
            $data->description = $request->input('description');
            $data->id_user = $request->input('idUser');
            $data->save();

            return response()->json($data);
    
        } catch (\Exception $e) {
            return response()->json($e);
        }
    }

    public function update(Request $request){
        try {
            $data = IntranetSection::findOrFail($request->input('idSection'));
            $data->id_department = $request->input('idDepartment');
            $data->name = $request->input('name');
            $data->description = $request->input('description');
            $data->id_user = $request->input('idUser');
            $data->save();
            return response()->json($data);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json(['error' => 'No se encontro la solicitud, asegurese de que el id enviado sea correcto.'], 404);
        } catch (\Exception $e) {
            return response()->json($e);
        }
    }
    
    public function delete(Request $request){
        try {
            $data = IntranetSection::findOrFail($request->input('idSection'));
            $data->status = '0';
            $data->id_user = $request->input('idUser');
            $data->save();
            return response()->json($data);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json(['error' => 'No se encontro la solicitud, asegurese de que el id enviado sea correcto.'], 404);
        } catch (\Exception $e) {
            return response()->json($e);
        }
    }
}

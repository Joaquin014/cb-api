<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use App\Client;
use App\ClientInvoicing;
use App\Unit;
use App\ClientQuotation;
use App\CreditCollection;
use App\Payments;
use App\SmsSender;
use App\Guide;
use App\Executive;
use Carbon\Carbon;
use DB;

class OrdersClientsEscheduleInstallationController extends Controller {

    public function store(Request $request){
        try {            
            
            $IDC = $request->input('idClient');
            $IDCI = $request->input('idInvoicing');
            
            if($IDC > 0){
                $data = Client::findOrFail($IDC);
                $data->name = $request->input('name');
                $data->last_name_paternal = $request->input('lastNamePaternal');
                $data->last_name_maternal = $request->input('lastNameMaternal');
                $data->email = $request->input('email');
                $data->phone = $request->input('phone');
                $data->country = $request->input('country');
                $data->state = $request->input('state');
                $data->city = $request->input('city');
                $data->postal_code = $request->input('postalCode');
                $data->street = $request->input('street');
                $data->number_interior = $request->input('numberInterior');
                $data->number_outdoor = $request->input('numberOutdoor');
                $data->suburb = $request->input('suburb');
                $data->invoice = $request->input('invoice');
                $data->special = $request->input('special');
                $data->id_user = $request->input('idUser');
                $data->save();

                if($request->input('invoice') == 1 && $IDCI > 0){
                    $clientInv = $request->input('clientInvoicing');
                    $data_ci = new ClientInvoicing;
                    $data_ci = ClientInvoicing::findOrFail($IDCI);
                    $data_ci->business_name = $clientInv['businessName'];
                    $data_ci->rfc = $clientInv['rfc'];
                    $data_ci->email = $clientInv['emailInvoice'];
                    $data_ci->sfdi = $clientInv['sfdi'];
                    $data_ci->country = $clientInv['country'];
                    $data_ci->state = $clientInv['state'];
                    $data_ci->city = $clientInv['city'];
                    $data_ci->postal_code = $clientInv['postalCode'];
                    $data_ci->street = $clientInv['street'];
                    $data_ci->number_interior = $clientInv['numberInterior'];
                    $data_ci->number_outdoor = $clientInv['numberOutdoor'];
                    $data_ci->suburb = $clientInv['suburb'];
                    $data_ci->id_user = $request->input('idUser');
                    $data_ci->save();

                } else if($request->input('invoice') == 1) {
                    $clientInv = $request->input('clientInvoicing');
                    $data_ci = new ClientInvoicing;
                    $data_ci->id_client = $IDC;
                    $data_ci->business_name = $clientInv['businessName'];
                    $data_ci->rfc = $clientInv['rfc'];
                    $data_ci->email = $clientInv['emailInvoice'];
                    $data_ci->sfdi = $clientInv['sfdi'];
                    $data_ci->country = $clientInv['country'];
                    $data_ci->state = $clientInv['state'];
                    $data_ci->city = $clientInv['city'];
                    $data_ci->postal_code = $clientInv['postalCode'];
                    $data_ci->street = $clientInv['street'];
                    $data_ci->number_interior = $clientInv['numberInterior'];
                    $data_ci->number_outdoor = $clientInv['numberOutdoor'];
                    $data_ci->suburb = $clientInv['suburb'];
                    $data_ci->id_user = $request->input('idUser');
                    $data_ci->save();
                    $IDCI = $data_ci->id;
                }                

            } else {
                $data = new Client;
                $data->name = $request->input('name');
                $data->last_name_paternal = $request->input('lastNamePaternal');
                $data->last_name_maternal = $request->input('lastNameMaternal');
                $data->email = $request->input('email');
                $data->phone = $request->input('phone');
                $data->country = $request->input('country');
                $data->state = $request->input('state');
                $data->city = $request->input('city');
                $data->postal_code = $request->input('postalCode');
                $data->street = $request->input('street');
                $data->number_interior = $request->input('numberInterior');
                $data->number_outdoor = $request->input('numberOutdoor');
                $data->suburb = $request->input('suburb');
                $data->invoice = $request->input('invoice');
                $data->special = $request->input('special');
                $data->id_user = $request->input('idUser');
                $data->save();
                $IDC = $data->id;


                if($request->input('invoice') == 1){
                  $clientInv = $request->input('clientInvoicing');
                  $data_ci = new ClientInvoicing;
                  $data_ci->id_client = $IDC;
                  $data_ci->business_name = $clientInv['businessName'];
                  $data_ci->rfc = $clientInv['rfc'];
                  $data_ci->email = $clientInv['emailInvoice'];
                  $data_ci->sfdi = $clientInv['sfdi'];
                  $data_ci->country = $clientInv['country'];
                  $data_ci->state = $clientInv['state'];
                  $data_ci->city = $clientInv['city'];
                  $data_ci->postal_code = $clientInv['postalCode'];
                  $data_ci->street = $clientInv['street'];
                  $data_ci->number_interior = $clientInv['numberInterior'];
                  $data_ci->number_outdoor = $clientInv['numberOutdoor'];
                  $data_ci->suburb = $clientInv['suburb'];
                  $data_ci->id_user = $request->input('idUser');
                  $data_ci->save();
                  $IDCI = $data_ci->id;
                }
            }

            $typeContract = "VENTA";
            if($request->input('typeStock') == 'COMODATO'){
                $typeContract = "COMODATO";
            }

            $expiration = Carbon::now()->addMonths(1);
            if($request->input('period') == 'ANUAL'){
                $expiration = Carbon::now()->addYears(1);
            }

            $data_cc = new CreditCollection;
            $data_cc->id_client = $IDC;
            $data_cc->id_company_invoices = 1;
            $data_cc->id_executive = $request->input('idExecutive');
            $data_cc->type_contract = $typeContract;
            $data_cc->service = "VERIFICAR";
            $data_cc->period = $request->input('period');
            $data_cc->date_contract = $request->input('date');
            $data_cc->date_expiration = $expiration;
            $data_cc->comment = $request->input('comments');
            $data_cc->subtotal = $request->input('subtotal');
            $data_cc->comission = $request->input('comission');
            $data_cc->total = $request->input('total');
            $data_cc->id_user = $request->input('idUser');
            $data_cc->save();
            $IDCC = $data_cc->id;

            $clientInst = $request->input('clientInstallation');
            $data_u = new Unit;
            $data_u->id_client = $IDC;
            $data_u->id_credit_collection = $IDCC;
            $data_u->date_scheduled = $clientInst['dateScheduled'];
            $data_u->time_scheduled = $clientInst['timeScheduled'];
            $data_u->plaque = $clientInst['plaque'];
            $data_u->brand = $clientInst['brand'];
            $data_u->model = $clientInst['model'];
            $data_u->color = $clientInst['color'];
            $data_u->serie = $clientInst['serie'];
            $data_u->installation = 1;
            $data_u->country = $clientInst['country'];
            $data_u->state = $clientInst['state'];
            $data_u->city = $clientInst['city'];
            $data_u->postal_code = $clientInst['postalCode'];
            $data_u->street = $clientInst['street'];
            $data_u->number_interior = $clientInst['numberInterior'];
            $data_u->number_outdoor = $clientInst['numberOutdoor'];
            $data_u->suburb = $clientInst['suburb'];
            $data_u->id_user = $request->input('idUser');
            $data_u->save();
            $IDCU = $data_u->id;

            $gpsEquipment = $request->input('gpsEquipment');
            foreach($gpsEquipment as $row){
                $data_e = new ClientQuotation;
                $data_e->id_client = $IDC;
                $data_e->id_credit_collection = $IDCC;
                $data_e->id_equipment = $row['idEquipment'];
                $data_e->quantity = $row['quantity'];
                $data_e->price = $row['price'];
                $data_e->comission = $row['montly_payment'];
                $data_e->id_user = $request->input('idUser');
                $data_e->save();
            }

            $sms = new SmsSender;
            $sms->number = "5516568325"; //num Bervic Locmex
            $sms->message = "Hola! Se ha programado una nueva instalación.";
            $sms->save();

            return response()->json($data);
    
        } catch (\Exception $e) {
            return response()->json($e);
        }
    }

    public function createNewClient(Request $request){
        try {     
            $phone = $request->input('phone');
            $query = "SELECT id FROM clients WHERE phone = '{$phone}'";
            $result = DB::select($query);

            if(count($result) > 0){
                $data = ["error" => "El cliente ya se encuentra registrado."];
            } else {
                $data = new Client;
                $data->name = $request->input('name');
                $data->last_name_paternal = $request->input('lastNamePaternal');
                $data->last_name_maternal = $request->input('lastNameMaternal');
                $data->email = $request->input('email');
                $data->phone = $request->input('phone');
                $data->country = $request->input('country');
                $data->state = $request->input('state');
                $data->city = $request->input('city');
                $data->postal_code = $request->input('postalCode');
                $data->street = $request->input('street');
                $data->number_interior = $request->input('numberInterior');
                $data->number_outdoor = $request->input('numberOutdoor');
                $data->suburb = $request->input('suburb');
                $data->invoice = $request->input('invoice');
                $data->special = $request->input('special');
                $data->id_user = $request->input('idUser');
                $data->save();
                $IDC = $data->id;


                if($request->input('invoice') == 1){
                  $clientInv = $request->input('clientInvoicing');
                  $data_ci = new ClientInvoicing;
                  $data_ci->id_client = $IDC;
                  $data_ci->business_name = $clientInv['businessName'];
                  $data_ci->rfc = $clientInv['rfc'];
                  $data_ci->email = $clientInv['emailInvoice'];
                  $data_ci->sfdi = $clientInv['sfdi'];
                  $data_ci->country = $clientInv['country'];
                  $data_ci->state = $clientInv['state'];
                  $data_ci->city = $clientInv['city'];
                  $data_ci->postal_code = $clientInv['postalCode'];
                  $data_ci->street = $clientInv['street'];
                  $data_ci->number_interior = $clientInv['numberInterior'];
                  $data_ci->number_outdoor = $clientInv['numberOutdoor'];
                  $data_ci->suburb = $clientInv['suburb'];
                  $data_ci->id_user = $request->input('idUser');
                  $data_ci->save();
                  $IDCI = $data_ci->id;
                }
            }
            
            return response()->json($data);
    
        } catch (\Exception $e) {
            return response()->json($e);
        }
    }
    
    public function saveVoucher(Request $request){
        try {
            $data = new Payments;
            $data->id_credit_collection = $request->input('idCreditCollection');
            $data->date_payment = Carbon::now();
            $data->month = Carbon::now()->month;
            $data->id_user = $request->input('idUser');
            $data->save();

            return response()->json($data);
    
        } catch (\Exception $e) {
            return response()->json($e);
        }
    }

    public function saveVoucherUrl(Request $request){
        try {
            $data = Payments::findOrFail($request->input('idPayment'));
            $data->voucher = $request->input('voucherUrl');
            $data->save();
            return response()->json($data);
            
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json(['error' => 'No se encontro la solicitud, asegurese de que el id enviado sea correcto.'], 404);
        } catch (\Exception $e) {
            return response()->json($e);
        }
    }

    public function saveGuide(Request $request){
        try {
            $data = new Guide;
            $data->id_credit_collection = $request->input('idCreditCollection');
            $data->id_user = $request->input('idUser');
            $data->save();

            $cc = CreditCollection::findOrFail($request->input('idCreditCollection'));
            $cc->service = "VIGENTE";
            $cc->save();

            return response()->json($data);
    
        } catch (\Exception $e) {
            return response()->json($e);
        }
    }

    public function saveGuideUrl(Request $request){
        try {
            $data = Guide::findOrFail($request->input('idGuide'));
            $data->guide = $request->input('guideUrl');
            $data->save();
            return response()->json($data);
            
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json(['error' => 'No se encontro la solicitud, asegurese de que el id enviado sea correcto.'], 404);
        } catch (\Exception $e) {
            return response()->json($e);
        }
    }
    
    public function saveStatusOrder(Request $request){
        $status = "";
        $number = "";
        $message = "";

        if($request->input('status') == 'VERIFICAR'){
           if($request->input('typeStock') == 'VENTA') {
                $status = "CARTA";
           } else {
                $status = "POR PAGAR";
           }
        } else if($request->input('status') == 'POR PAGAR'){
            $status = "CARTA";
            $number = "5516568325"; //num Bervic Locmex
            $message = "Hola! Hay una carta de movilidad por liberar.";
        } else if($request->input('status') == 'CARTA'){
            $status = "VIGENTE";
        }

        try {
            $data = CreditCollection::findOrFail($request->input('idCreditCollection'));
            $data->service = $status;
            if($request->input('observations') != '') $data->observations = $request->input('observations');
            if($request->input('amount') != '') $data->amount = $request->input('amount');
            if($request->input('mobilityCard') == 1) $data->mobility_card = '1';
            $data->save();

            if($number != '' && $message != ''){
                $sms = new SmsSender;
                $sms->number = $number;
                $sms->message = $message;
                $sms->save();
            }   
            
            return response()->json($data);
            
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json(['error' => 'No se encontro la solicitud, asegurese de que el id enviado sea correcto.'], 404);
        } catch (\Exception $e) {
            return response()->json($e);
        }
    }
    
    public function get($id_permission){
      try { 
          $query =  Client::
                      join('credit_collection', 'credit_collection.id_client', '=', 'clients.id')
                      ->join('config_executives', 'config_executives.id', '=', 'credit_collection.id_executive')
                      ->select(
                          'clients.id', 
                          'clients.name', 
                          'clients.last_name_paternal',
                          'clients.last_name_maternal',
                          'config_executives.name AS name_executive',
                          'config_executives.last_name AS last_name_executive',
                          'config_executives.type_stock AS type_stock_executive',
                          'credit_collection.service AS status_order',
                          DB::raw("DATE_FORMAT(clients.created_at, '%d/%m/%Y') as date"),
                          DB::raw("DATE_FORMAT(clients.created_at, '%H:%i:%s') as hour"),
                          'credit_collection.id as id_credit_collection')
                      ->where('clients.status', '=', 1);
          
          if($id_permission == 13){
              $data = $query
                ->where('credit_collection.service', '=', 'VERIFICAR')
                ->orWhere('credit_collection.service', '=', 'POR PAGAR')
                ->orderBy('credit_collection.id', 'desc')->get();
          } else if($id_permission == 15 || $id_permission == 17){
            $data = $query
            ->where('credit_collection.service', '=', 'CARTA')
            ->orWhere('credit_collection.service', '=', 'POR PAGAR')
            ->orderBy('credit_collection.id', 'desc')->get();
          } else {
              $data = $query
                ->where('credit_collection.service', '=', 'CARTA')
                ->orWhere('credit_collection.service', '=', 'VIGENTE')
                ->orWhere('credit_collection.service', '=', 'POR PAGAR')
                ->orderBy('credit_collection.id', 'desc')->get();
          }
                  
          return response()->json($data);
  
      } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
          return response()->json(['error' => 'No se encontraron resultados de la consulta.'], 404);
      } catch (\Exception $e) {
          return response()->json($e);
      }
    }

    public function getDetails(Request $request){
        $response = new \stdClass;

        try { 
            $client = 
                Client::
                    join('credit_collection', 'credit_collection.id_client', '=', 'clients.id')
                    ->join('citys', 'citys.id', '=', 'clients.city')
                    ->join('states', 'states.id', '=', 'clients.state')
                    ->join('countrys', 'countrys.id', '=', 'clients.country')
                    ->select('clients.id', 
                            'clients.name', 
                            'clients.last_name_paternal', 
                            'clients.last_name_maternal',
                            'clients.email',
                            'clients.phone',
                            'citys.name as city',
                            'states.name as state',
                            'countrys.name as country',
                            'clients.postal_code',
                            'clients.street',
                            'clients.number_interior',
                            'clients.number_outdoor',
                            'clients.suburb',
                            'clients.invoice',
                            'clients.special',
                            'credit_collection.comment as comments',
                            'credit_collection.subtotal',
                            'credit_collection.comission',
                            'credit_collection.total',
                            'credit_collection.id as id_credit_collection')
                    ->where('credit_collection.id', '=', $request->input('idCreditCollection'))
                    ->get();

                $payments = 
                    Payments::
                        join('credit_collection', 'credit_collection.id', '=', 'payments.id_credit_collection')
                        ->select('payments.id', 
                                'payments.date_payment', 
                                'payments.month', 
                                'payments.type_payment',
                                'payments.voucher')
                        ->where('credit_collection.id', '=', $request->input('idCreditCollection'))
                        ->get();
                
                $guides = 
                        Guide::
                            join('credit_collection', 'credit_collection.id', '=', 'guides.id_credit_collection')
                            ->select('guides.id', 
                                    'guides.guide')
                            ->where('credit_collection.id', '=', $request->input('idCreditCollection'))
                            ->get();

                $clientInvoicing = 
                    ClientInvoicing::
                        join('citys', 'citys.id', '=', 'clients_invoicing.city')
                        ->join('states', 'states.id', '=', 'clients_invoicing.state')
                        ->join('countrys', 'countrys.id', '=', 'clients_invoicing.country')
                        ->select('clients_invoicing.id', 
                                'clients_invoicing.business_name',
                                'clients_invoicing.rfc',
                                'clients_invoicing.email',
                                'clients_invoicing.sfdi',
                                'citys.name as city',
                                'states.name as state',
                                'countrys.name as country',
                                'clients_invoicing.postal_code',
                                'clients_invoicing.street',
                                'clients_invoicing.number_interior',
                                'clients_invoicing.number_outdoor',
                                'clients_invoicing.suburb')
                        ->where('clients_invoicing.id_client', '=', $client[0]->id)
                        ->get();
                
                $unit = 
                    Unit::
                        join('citys', 'citys.id', '=', 'clients_units.city')
                        ->join('states', 'states.id', '=', 'clients_units.state')
                        ->join('countrys', 'countrys.id', '=', 'clients_units.country')
                        ->select('clients_units.id', 
                                DB::raw("DATE_FORMAT(clients_units.date_scheduled, '%d/%m/%Y') as date_scheduled"),
                                DB::raw("DATE_FORMAT(clients_units.time_scheduled, '%H:%i') as time_scheduled"),
                                'clients_units.plaque',
                                'clients_units.brand',
                                'clients_units.model',
                                'clients_units.serie',
                                'clients_units.color',
                                'citys.name as city',
                                'states.name as state',
                                'countrys.name as country',
                                'clients_units.postal_code',
                                'clients_units.street',
                                'clients_units.number_interior',
                                'clients_units.number_outdoor',
                                'clients_units.suburb')
                        ->where('clients_units.id_client', '=', $client[0]->id)
                        ->where('clients_units.id_credit_collection', '=', $client[0]->id_credit_collection)
                        ->get();

                $equipments = 
                    ClientQuotation::
                        join('products_packages', 'products_packages.id', '=', 'clients_quotation.id_equipment')
                        ->select('clients_quotation.id', 
                                'products_packages.id as id_equipment',
                                'products_packages.name as equipment',
                                'products_packages.description',
                                'clients_quotation.quantity',
                                'clients_quotation.price',
                                'clients_quotation.comission')
                        ->where('clients_quotation.id_client', '=', $client[0]->id)
                        ->where('clients_quotation.id_credit_collection', '=', $client[0]->id_credit_collection)
                        ->get();
            
            $response->client = $client[0];
            if(count($clientInvoicing) > 0){
                $response->clientInvoicing = $clientInvoicing[0];
            } else {
                $response->clientInvoicing = [];
            }

            if(count($unit) > 0){
                $response->unitInstallation = $unit[0];
            } else {
                $response->unitInstallation = [];
            }

            $response->payments = $payments;
            $response->guides = $guides;
            $response->equipments = $equipments;
            return response()->json($response);
    
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json(['error' => 'No se encontraron resultados de la consulta.'], 404);
        } catch (\Exception $e) {
            return response()->json($e);
        }
    }
    
    
    //lista todo los clientes
    public function getClients(Request $request){ 
        $response = new \stdClass;
        try{
            $clients = Client::
                        join('citys', 'citys.id', '=', 'clients.city')
                        ->join('states', 'states.id', '=', 'clients.state')
                        ->join('countrys', 'countrys.id', '=','clients.country')
                        ->select(
                            'clients.id', 
                            'clients.name', 
                            'clients.last_name_paternal',
                            'clients.last_name_maternal',
                            'clients.phone',
                            'clients.email',
                            'clients.invoice as facturacion',
                            'clients.special',
                            'citys.id as city',
                            'citys.name as name_city',
                            'states.id as state',
                            'states.name as name_state',
                            'countrys.id as country',
                            'clients.street',
                            'clients.number_interior',
                            'clients.number_outdoor',
                            'clients.suburb',
                            'clients.postal_code')
                        ->where('clients.status', '=', 1)
                        ->get();

                    /* $invoice = ClientInvoicing::
                        join('citys', 'citys.id', '=', 'clients_invoicing.city')
                        ->join('states', 'states.id', '=', 'clients_invoicing.state')
                        ->join('countrys', 'countrys.id', '=','clients_invoicing.country')
                        ->select(
                            'clients_invoicing.id', 
                            'clients_invoicing.business_name', 
                            'clients_invoicing.rfc',
                            'clients_invoicing.email',
                            'clients_invoicing.phone',
                            'clients_invoicing.sfdi',
                            'citys.id as city',
                            'states.id as state',
                            'countrys.id as country',
                            'clients_invoicing.street',
                            'clients_invoicing.number_interior',
                            'clients_invoicing.number_outdoor',
                            'clients_invoicing.suburb',
                            'clients_invoicing.postal_code')
                        ->where('clients_invoicing.status', '=', 1)
                        ->where('clients_invoicing.id_client', '=', $client[0]->id)
                        ->get();  */

                $response->clients = $clients;
                return response()->json($response);
        } catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json(['error' => 'No se encontraron resultados de la consulta.'], 404);
        } catch (\Exception $e) {
            return response()->json($e);
        }
    }
    public function updateClient(Request $request){
        try {
            $IDC = $request->input('idClient');
            $IDCI = $request->input('idInvoicing');
            $query = "SELECT id FROM clients WHERE phone = '{$phone}'";
            $result = DB::select($query);

            /* $result = DB::table('clients')
                        ->updateOrInsert(
                            [$request->input('name'),]
                            
                        );
            $query = "UPDATE clients set name=''" */
            if($IDC > 0){
                $data = Client::findOrFail($IDC);
                $data->name = $request->input('name');
                $data->last_name_paternal = $request->input('lastNamePaternal');
                $data->last_name_maternal = $request->input('lastNameMaternal');
                $data->email = $request->input('email');
                $data->phone = $request->input('phone');
                $data->country = $request->input('country');
                $data->state = $request->input('state');
                $data->city = $request->input('city');
                $data->postal_code = $request->input('postalCode');
                $data->street = $request->input('street');
                $data->number_interior = $request->input('numberInterior');
                $data->number_outdoor = $request->input('numberOutdoor');
                $data->suburb = $request->input('suburb');
                $data->invoice = $request->input('invoice');
                $data->special = $request->input('special');
                $data->id_user = $request->input('idUser');
                $data->save();

                if($request->input('invoice') == 1 && $IDCI > 0){
                    $clientInv = $request->input('clientInvoicing');
                    $data_ci = new ClientInvoicing;
                    $data_ci = ClientInvoicing::findOrFail($IDCI);
                    $data_ci->business_name = $clientInv['businessName'];
                    $data_ci->rfc = $clientInv['rfc'];
                    $data_ci->email = $clientInv['emailInvoice'];
                    $data_ci->sfdi = $clientInv['sfdi'];
                    $data_ci->country = $clientInv['country'];
                    $data_ci->state = $clientInv['state'];
                    $data_ci->city = $clientInv['city'];
                    $data_ci->postal_code = $clientInv['postalCode'];
                    $data_ci->street = $clientInv['street'];
                    $data_ci->number_interior = $clientInv['numberInterior'];
                    $data_ci->number_outdoor = $clientInv['numberOutdoor'];
                    $data_ci->suburb = $clientInv['suburb'];
                    $data_ci->id_user = $request->input('idUser');
                    $data_ci->save();

                } else if($request->input('invoice') == 1) {
                    $clientInv = $request->input('clientInvoicing');
                    $data_ci = new ClientInvoicing;
                    $data_ci->id_client = $IDC;
                    $data_ci->business_name = $clientInv['businessName'];
                    $data_ci->rfc = $clientInv['rfc'];
                    $data_ci->email = $clientInv['emailInvoice'];
                    $data_ci->sfdi = $clientInv['sfdi'];
                    $data_ci->country = $clientInv['country'];
                    $data_ci->state = $clientInv['state'];
                    $data_ci->city = $clientInv['city'];
                    $data_ci->postal_code = $clientInv['postalCode'];
                    $data_ci->street = $clientInv['street'];
                    $data_ci->number_interior = $clientInv['numberInterior'];
                    $data_ci->number_outdoor = $clientInv['numberOutdoor'];
                    $data_ci->suburb = $clientInv['suburb'];
                    $data_ci->id_user = $request->input('idUser');
                    $data_ci->save();
                    $IDCI = $data_ci->id;
                }                

            } else {
                $data = new Client;
                $data->name = $request->input('name');
                $data->last_name_paternal = $request->input('lastNamePaternal');
                $data->last_name_maternal = $request->input('lastNameMaternal');
                $data->email = $request->input('email');
                $data->phone = $request->input('phone');
                $data->country = $request->input('country');
                $data->state = $request->input('state');
                $data->city = $request->input('city');
                $data->postal_code = $request->input('postalCode');
                $data->street = $request->input('street');
                $data->number_interior = $request->input('numberInterior');
                $data->number_outdoor = $request->input('numberOutdoor');
                $data->suburb = $request->input('suburb');
                $data->invoice = $request->input('invoice');
                $data->special = $request->input('special');
                $data->id_user = $request->input('idUser');
                $data->save();
                $IDC = $data->id;


                if($request->input('invoice') == 1){
                  $clientInv = $request->input('clientInvoicing');
                  $data_ci = new ClientInvoicing;
                  $data_ci->id_client = $IDC;
                  $data_ci->business_name = $clientInv['businessName'];
                  $data_ci->rfc = $clientInv['rfc'];
                  $data_ci->email = $clientInv['emailInvoice'];
                  $data_ci->sfdi = $clientInv['sfdi'];
                  $data_ci->country = $clientInv['country'];
                  $data_ci->state = $clientInv['state'];
                  $data_ci->city = $clientInv['city'];
                  $data_ci->postal_code = $clientInv['postalCode'];
                  $data_ci->street = $clientInv['street'];
                  $data_ci->number_interior = $clientInv['numberInterior'];
                  $data_ci->number_outdoor = $clientInv['numberOutdoor'];
                  $data_ci->suburb = $clientInv['suburb'];
                  $data_ci->id_user = $request->input('idUser');
                  $data_ci->save();
                  $IDCI = $data_ci->id;
                }
            }



            return response()->json($data);
        
        } catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json(['error' => 'No se encontro la solicitud, asegurese de que el id enviado sea correcto.'], 404);
        } catch (\Exception $e) {
            return response()->json($e);
        }
    }
    public function rem(Request $request){
        try {
            $data = Client::where('id', '=', $request->input('idClient'))->update(['status' => 0]);
            
            if($data == 1)
              $data = true;
            else 
              $data = false;
            
            return response()->json($data);
    
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json(['error' => 'No se encontro la solicitud, asegurese de que el id enviado sea correcto.'], 404);
        
        } catch (\Exception $e) {
            return response()->json($e);
        }
      }
    
    

    public function getClient(Request $request){
        $response = new \stdClass;
        try { 
            $id = Client::select('id')->where('phone', '=', $request->input('phone'))->get();
            
            
            if(count($id) > 0){
                $client = Client::
                        join('citys', 'citys.id', '=', 'clients.city')
                        ->join('states', 'states.id', '=', 'clients.state')
                        ->join('countrys', 'countrys.id', '=','clients.country')
                        ->select(
                            'clients.id', 
                            'clients.name', 
                            'clients.last_name_paternal',
                            'clients.last_name_maternal',
                            'clients.email',
                            'clients.phone',
                            'clients.invoice',
                            'clients.special',
                            'citys.id as city',
                            'citys.name as name_city',
                            'states.id as state',
                            'states.name as name_state',
                            'countrys.id as country',
                            'countrys.name as name_country',
                            'clients.street',
                            'clients.number_interior',
                            'clients.number_outdoor',
                            'clients.suburb',
                            'clients.postal_code')
                        ->where('clients.status', '=', 1)
                        ->where('clients.phone', '=', $request->input('phone'))
                        ->get();

                $invoice = ClientInvoicing::
                        join('citys', 'citys.id', '=', 'clients_invoicing.city')
                        ->join('states', 'states.id', '=', 'clients_invoicing.state')
                        ->join('countrys', 'countrys.id', '=','clients_invoicing.country')
                        ->select(
                            'clients_invoicing.id', 
                            'clients_invoicing.business_name', 
                            'clients_invoicing.rfc',
                            'clients_invoicing.email',
                            'clients_invoicing.phone',
                            'clients_invoicing.sfdi',
                            'citys.id as city',
                            'citys.name as name_city',
                            'states.id as state',
                            'states.name as name_state',
                            'countrys.id as country',
                            'countrys.name as name_country',
                            'clients_invoicing.street',
                            'clients_invoicing.number_interior',
                            'clients_invoicing.number_outdoor',
                            'clients_invoicing.suburb',
                            'clients_invoicing.postal_code')
                        ->where('clients_invoicing.status', '=', 1)
                        ->where('clients_invoicing.id_client', '=', $client[0]->id)
                        ->get();

                $response->client = $client;
                $response->client = $client[0];
                if(count($invoice) > 0){
                    $response->clientInvoicing = $invoice[0];
                } else {
                    $response->clientInvoicing = [];
                }

                return response()->json($response);
            } else {
                return response()->json(['error' => 'El cliente no está registrado.'], 200);
            }
                    
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json(['error' => 'No se encontraron resultados de la consulta.'], 404);
        } catch (\Exception $e) {
            return response()->json($e);
        }
    }

    public function getUpPayments($id_user){
        try { 
            $asesor = Executive::select('id')->where('id_user', '=', $id_user)->get();


            $data =  Client::
                      join('credit_collection', 'credit_collection.id_client', '=', 'clients.id')
                      ->join('config_executives', 'config_executives.id', '=', 'credit_collection.id_executive')
                      ->select(
                          'clients.id', 
                          'clients.name', 
                          'clients.last_name_paternal',
                          'clients.last_name_maternal',
                          'config_executives.name AS name_executive',
                          'config_executives.last_name AS last_name_executive',
                          'credit_collection.service AS status_order',
                          DB::raw("DATE_FORMAT(clients.created_at, '%d/%m/%Y') as date"),
                          DB::raw("DATE_FORMAT(clients.created_at, '%H:%i:%s') as hour"),
                          'credit_collection.id as id_credit_collection')
                      ->where('clients.status', '=', 1)
                      ->where('credit_collection.service', '=', 'POR PAGAR')
                      ->where('credit_collection.id_executive', '=', $asesor[0]->id)
                      ->orderBy('credit_collection.id', 'desc')
                      ->get();

            return response()->json($data);
    
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json(['error' => 'No se encontraron resultados de la consulta.'], 404);
        } catch (\Exception $e) {
            return response()->json($e);
        }
    }
}

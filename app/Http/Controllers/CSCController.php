<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Validator;

use App\City;
use App\Country;
use App\State;

use DB;

class CSCController extends Controller
{
    public function index()
    {
        $countrys = Country::select('name', 'id', 'timestamp')->where('status', 1)->orderBy('name', 'asc')->get();
        $states = State::select('name', 'id', 'timestamp', "id_country")->where('status', 1)->orderBy('name', 'asc')->get();
        $citys = City::select('name', 'id', 'timestamp', "id_country", "id_state")->where('status', 1)->orderBy('name', 'asc')->get();

        $response = new \stdClass;

        $response->countrys = $countrys;
        $response->states = $states;
        $response->citys = $citys;
        
        return response()->json($response);        
    }
}

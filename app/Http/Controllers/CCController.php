<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Client;
use App\ClientInvoicing;
use App\Unit;
use App\Equipment;
use App\CreditCollection;
use App\Payment;

use DB;


class CCController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $response = new \stdClass;
        $response->status = 400;
        //$response->params = $params;

        $creditCollection = "
            Ejecutiva ID : $request->input('executive') 
            Empresa que factura ID : $request->input('invoicingCompany') 
            Cliente ID : $request->input('clientName') 
            Tipo de Contrato : $request->input('typeContract') 
            Periodo : $request->input('period') 
            Fecha Vencimiento : $request->input('dateExpiration') 
            Costo de Servicio : $request->input('costService') 
            Costo de Instalación : $request->input('costInstallation');";

        $clients = "
            Nombre : $request->input('name') 
            Apellido paterno : $request->input('lastNamePaternal') 
            Apellido materno : $request->input('lastNameMaternal') 
            Número de unidades : $request->input('numberUnits') 
            Correo : $request->input('email') 
            País : $request->input('country') 
            Estado : $request->input('state') 
            Ciudad : $request->input('city') 
            Código postal : $request->input('postalCode') 
            Calle : $request->input('street') 
            Número exterior : $request->input('numberOutdoor') 
            Número interior : $request->input('numberInterior') 
            Colonia : $request->input('suburb') 
            Teléfono : $request->input('phone') 
            Factura : $request->input('invoice');";

        $clientsInvoicing = "
            Razon social : $request->input('businessName') 
            RFC : $request->input('rfc') 
            Correo : $request->input('email');";

        if($request->input('event') == 'save'){
            $data = new Client;
            $data->name = $request->input('name');
            $data->last_name_paternal  = $request->input('lastNamePaternal');
            $data->last_name_maternal  = $request->input('lastNameMaternal');
            $data->number_units  = $request->input('numberUnits');
            $data->email  = $request->input('email');
            $data->country  = $request->input('country');
            $data->state  = $request->input('state');
            $data->city  = $request->input('city');
            $data->postal_code  = $request->input('postalCode');
            $data->street  = $request->input('street');
            $data->number_interior  = $request->input('numberInterior');
            $data->number_outdoor  = $request->input('numberOutdoor');
            $data->suburb  = $request->input('suburb');
            $data->phone  = $request->input('phone');
            $data->invoice  = $request->input('invoice');
            $data->id_user  = $request->input('idUser');
            $data->save();
            $IDC = $data->id;
            app('App\Http\Controllers\LogController')->store($request->input("event"), $request->input('idUser'), 'clients', $IDC, $clients);
    
            $data_ci = new ClientInvoicing;
            $data_ci->id_client = $IDC;
            $data_ci->business_name  = $request->input('businessName');
            $data_ci->rfc  = $request->input('rfc');
            $data_ci->email  = $request->input('email');
            
            $data_ci->save();
            $IDCI = $data_ci->id;
            app('App\Http\Controllers\LogController')->store($request->input("event"), $request->input('idUser'), 'clients_invoicing', $IDCI, $clientsInvoicing);
            
            $units = $request->input('units');
            foreach($units as $row){
                $data_u = new Units;
                $data_u->id_client = $IDC;
                $data_u->plaque  = $row->plaque;
                $data_u->brand  = $row->brand;
                $data_u->model  = $row->model;
                $data_u->color  = $row->color;
                $data_u->installation  = $row->installation;
                $data_u->id_user  = $row->idUser;
                $data_u->save();
                $IDCU = $data_u->id;

                $clientsUnits .= "
                    Placa : $row->plaque 
                    Marca : $row->brand 
                    Modelo : $row->model 
                    Color : $row->color 
                    Instalación : $row->installation;";
                app('App\Http\Controllers\LogController')->store($request->input("event"), $request->input('idUser'), 'clients_units', $IDCU, $clientsUnits);
            }

            $gpsEquipment = $request->input('gpsEquipment');
            foreach($gpsEquipment as $row){
                $data_e = new Equipment;
                $data_e->id_cliente = $IDC;
                $data_e->id_equipo  = $row->typeGpsEquipment;
                $data_e->cantidad  = $row->numberGpsEquiment;
                $data_e->equipo_parado  = $row->unitStandBy;
                $data_e->id_usuario  = $row->idUser;
                $data_e->save();
                $IDAE = $data_e->id;
                
                $assigmentEquipment = "
                    Cliente ID : $IDC
                    Equipo ID : $row->typeGpsEquipment
                    Cantidad : $row->numberGpsEquiment
                    Equipo parado : $row->unitStandBy";
                app('App\Http\Controllers\LogController')->store($request->input("event"), $request->input('idUser'), 'asignacion_equipos', $IDAE, $assigmentEquipment);
            }
            
            if($request->input('dateContract') != ''){
                $creditCollection .= ", Fecha Contrato : $request->input('dateContract')";
            }	
    
            if($request->input('invoicingCompany') != ''){
                $creditCollection .= ",  : $request->input('dateContract')";
            }
            
            $data_cc = new CreditCollection;
            $data_cc->id_client = $IDC;
            $data_cc->id_executive  = $row->executive;
            $data_cc->period  = $row->period;
            $data_cc->date_expiration  = $row->dateExpiration;
            $data_cc->cost_service  = $row->costService;
            $data_cc->cost_installation  = $row->costInstallation;
            $data_cc->comment  = $row->comment;
            $data_cc->id_user  = $row->idUser;
            if($request->input('dateContract') != ''){
                $data_cc->date_contract  = $request->input('dateContract');
            }
            if($request->input('invoicingCompany') != ''){
                $data_cc->id_company_invoices  = $request->input('invoicingCompany');
            }
            $data_cc->save();
            $IDCC = $data_cc->id;

            app('App\Http\Controllers\LogController')->store($request->input("event"), $request->input('idUser'), 'credit_collection', $IDCC, $creditCollection);
    
            if($IDCC > 0){
                $payments = $request->input('payments');
                foreach($payments as $row){
                    $data_p = new Payment;
                    $data_p->id_credit_collection = $IDCC;
                    $data_p->date_payment  = $row->paymentDate;
                    $data_p->payment  = $row->payment;
                    $data_p->month  = $row->month;
                    $data_p->type_payment  = 'REALIZADO';
                    $data_p->id_user  = $row->idUser;
                    if($request->input('clarification') != ''){
                        $data_p->id_clarification = $row->clarification;
                    }
                    $data_p->save();

                    $payments .= "
                            Credito y cobranza ID : $IDCC
                            Fecha de pago : $row->paymentDate
                            Pago : $row->payment
                            Mes : $row->month
                            Tipo de pago : $row->typePayment
                            Aclaracion ID : $row->clarification;";
                }
    
                app('App\Http\Controllers\LogController')->store($request->input("event"), $request->input('idUser'), 'payments', $IDCC, $payments);
                $response->status = 200;
            }	
        } else if($request->input('event') == 'query'){
            $clientes = array();
            $query_c = "SELECT 
                    cc.id,
                    cc.id_executive,
                    cc.id_company_invoices,
                    cc.type_contract,
                    DATE_FORMAT(cc.date_contract, '%d/%m/%Y') AS date_contract,
                    DATE_FORMAT(cc.date_expiration, '%d/%m/%Y') AS date_expiration,
                    cc.period,
                    cc.cost_service,
                    cc.cost_installation,
                    cli.id AS id_cliente,
                    cli.number_units,
                    cli.invoice,
                    cli.country,
                    cli.state,
                    cli.city,
                    cli.postal_code,
                    cli.street,
                    cli.number_outdoor,
                    cli.number_interior,
                    cli.suburb,
                    ci.email,
                    ci.rfc,
                    ci.business_name
                FROM credit_collection cc
                INNER JOIN clients cli ON cli.id = cc.id_client
                LEFT JOIN clients_invoicing ci ON ci.id_client = cc.id_client
                WHERE cli.id = ?
                GROUP BY cli.id;";
            $result_c = DB::select($query_c, array($request->input('idClient')));

            foreach($result_c as $key => $cli){
                $query_a = "SELECT 
                        cu.id,
                        cu.plaque,
                        cu.brand,
                        cu.model,
                        cu.color,
                        cu.serie,
                        cu.installation
                        FROM clients_units cu
                        INNER JOIN clients cli ON cli.id = cu.id_client
                        WHERE cu.status = 1 AND cli.id = ?";
                $result_a = DB::select($query_a, array($request->input('idClient')));

                foreach($result_a as $key => $uni){
                    $cli->units[] = $uni;
                }

                $query_b = "SELECT 
                        e.id,
                        ae.id as id_asignacion,
                        ae.cantidad,
                        ae.equipo_parado,
                        e.precio,
                        e.precio_parado,
                        ae.equipo_parado
                        FROM asignacion_equipos ae
                        INNER JOIN clients cli ON cli.id = ae.id_cliente
                        INNER JOIN config_equipos e ON e.id = ae.id_equipo
                        WHERE ae.estado = 1 AND cli.id = ?";
                $result_b = DB::select($query_b, array($request->input('idClient')));
                foreach($result_b as $key => $gps){
                    $cli->gpsEquipment[] = $gps;
                }
                $clientes[] = $cli;
            }
    
            if($clientes === null) $clientes = [];
            
            $response = sizeof($clientes) == 0 ? [] : $clientes[0];
        }
        return response()->json($response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

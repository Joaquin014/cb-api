<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use App\StockInventory;
use App\StockInventoryDetails;
use App\StockPayments;
use App\StockGuides;
use App\SmsSender;
use App\Executive;
use Carbon\Carbon;
use DB;

class OrdersClientsStockInventoryController extends Controller {

    public function store(Request $request){
        try {
            $data_si = new StockInventory;
            $data_si->id_executive = $request->input('idExecutive');
            $data_si->date = $request->input('date');
            $data_si->hour = $request->input('hour');
            $data_si->subtotal = $request->input('subtotal');
            $data_si->comission = $request->input('comission');
            $data_si->total = $request->input('total');
            $data_si->name_receives = $request->input('nameReceives');
            $data_si->phone_contact = $request->input('phoneContact');
            $data_si->street = $request->input('street');
            $data_si->number_interior = $request->input('numberInterior');
            $data_si->number_outdoor = $request->input('numberOutdoor');
            $data_si->suburb = $request->input('suburb');
            $data_si->comments = $request->input('comments');
            $data_si->city = $request->input('city');
            $data_si->state = $request->input('state');
            $data_si->country = $request->input('country');
            $data_si->postal_code = $request->input('postalCode');
            $data_si->id_user = $request->input('idUser');
            $data_si->save();

            $gpsEquipment = $request->input('gpsEquipment');
            foreach($gpsEquipment as $row){
                $data_e = new StockInventoryDetails;
                $data_e->id_stock = $data_si->id;
                $data_e->id_product = $row['idProduct'];
                $data_e->quantity = $row['quantity'];
                $data_e->price = $row['price'];
                $data_e->comission = $row['montly_payment'];
                $data_e->id_user = $request->input('idUser');
                $data_e->save();
            }

            $nameExecutive = Executive::select('name')->where('id', '=', $request->input('idExecutive'))->get()[0]->name;

            if($nameExecutive != ''){
                $message = "Buen día! el asesor ". $nameExecutive . " ha solicitado equipos y está en espera de la revisión de su pago.";
            } else {
                $message = "Buen día! Hay un nuevo pedido por verficar.";
            }

            $sms = new SmsSender;
            $sms->number = "6691430146"; //num Erika Locmex
            $sms->message = $message;
            $sms->save();
            
            return response()->json($data_si);
    
        } catch (\Exception $e) {
            return response()->json($e);
        }
    }

    public function saveVoucher(Request $request){
      try {
          $data = new StockPayments;
          $data->id_stock = $request->input('idStock');
          $data->date_payment = Carbon::now();
          $data->id_user = $request->input('idUser');
          $data->save();

          return response()->json($data);
  
      } catch (\Exception $e) {
          return response()->json($e);
      }
    }

    public function saveVoucherUrl(Request $request){
        try {
            $data = StockPayments::findOrFail($request->input('idPayment'));
            $data->voucher = $request->input('voucherUrl');
            $data->save();
            return response()->json($data);
            
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json(['error' => 'No se encontro la solicitud, asegurese de que el id enviado sea correcto.'], 404);
        } catch (\Exception $e) {
            return response()->json($e);
        }
    }

    public function saveGuide(Request $request){
        try {
            $data = new StockGuides;
            $data->id_stock = $request->input('idStock');
            $data->id_user = $request->input('idUser');
            $data->save();

            $cc = StockInventory::findOrFail($request->input('idStock'));
            $cc->status_stock = "SURTIDO";
            $cc->save();

            return response()->json($data);
    
        } catch (\Exception $e) {
            return response()->json($e);
        }
    }

    public function saveGuideUrl(Request $request){
        try {
            $data = StockGuides::findOrFail($request->input('idGuide'));
            $data->guide = $request->input('guideUrl');
            $data->save();
            return response()->json($data);
            
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json(['error' => 'No se encontro la solicitud, asegurese de que el id enviado sea correcto.'], 404);
        } catch (\Exception $e) {
            return response()->json($e);
        }
    }

    public function saveStatusOrder(Request $request){
        $status = "";
        $number = "";
        $message = "";

        if($request->input('status') == 'VERIFICAR'){
            $status = "ENVIAR";
            $number = "6692477766"; //num Carolina Locmex
            $message = "Nuevo envío! hay un aesor en espera del envío de su pedido.";
        } else if($request->input('status') == 'ENVIAR'){
            $status = "SURTIDO";
        } else if($request->input('status') == 'RECHAZADA'){
            $status = "VERIFICAR";
            $number = "6691430146"; //num Erika Locmex
            $message = "Buen día! Hay un pago subido nuevamente por verficar.";
        }

        try {
            $data = StockInventory::findOrFail($request->input('idStock'));
            $data->status_stock = $status;
            if($request->input('observations') != '') $data->observations = $request->input('observations');
            if($request->input('amount') != '') $data->amount = $request->input('amount');
            $data->save();

            if($message != '' && $number != ''){
                $sms = new SmsSender;
                $sms->number = $number;
                $sms->message = $message;
                $sms->save();
            }
            
            return response()->json($data);
            
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json(['error' => 'No se encontro la solicitud, asegurese de que el id enviado sea correcto.'], 404);
        } catch (\Exception $e) {
            return response()->json($e);
        }
    }

    public function reject(Request $request){
        try {
            $data = StockInventory::findOrFail($request->input('idStock'));
            $data->status_stock = "RECHAZADO";
            $data->observations = $request->input('observations');
            $data->save();
            
            return response()->json($data);
            
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json(['error' => 'No se encontro la solicitud, asegurese de que el id enviado sea correcto.'], 404);
        } catch (\Exception $e) {
            return response()->json($e);
        }
    }

    public function get($id_permission){
        try { 
            $query = StockInventory::
                      join('config_executives', 'config_executives.id', '=', 'stock_inventory.id_executive')
                      ->select(
                        'stock_inventory.id as id_stock',
                        DB::raw("DATE_FORMAT(stock_inventory.date, '%d/%m/%Y') as date"),
                        DB::raw("DATE_FORMAT(stock_inventory.hour, '%H:%i:%s') as hour"),
                        'stock_inventory.id_executive',
                        'stock_inventory.status_stock',
                        'config_executives.name as name_executive',
                        'config_executives.last_name as last_name_executive',
                        'config_executives.type_stock as type_stock_executive'
                      )
                      ->where('stock_inventory.status', '=', '1');

            if($id_permission == 13){
                $data = $query->where('stock_inventory.status_stock', '=', 'VERIFICAR')->orderBy('stock_inventory.id', 'desc')->get();
            } else if($id_permission == 14){
                $data = $query->where('stock_inventory.status_stock', '=', 'ENVIAR')->orderBy('stock_inventory.id', 'desc')->get();
            } else {
                $data = $query->where('stock_inventory.status_stock', '=', 'ENVIAR')->orWhere('stock_inventory.status_stock', '=', 'VERIFICAR')->orderBy('stock_inventory.id', 'desc')->get();
            }
                    
            return response()->json($data);
    
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json(['error' => 'No se encontraron resultados de la consulta.'], 404);
        } catch (\Exception $e) {
            return response()->json($e);
        }
    }

    public function getRequests($id_user){
        try { 
        
            $query = StockInventory::
                    join('config_executives', 'config_executives.id', '=', 'stock_inventory.id_executive')
                    ->select(
                        'stock_inventory.id as id_stock',
                        DB::raw("DATE_FORMAT(stock_inventory.date, '%d/%m/%Y') as date"),
                        DB::raw("DATE_FORMAT(stock_inventory.hour, '%H:%i:%s') as hour"),
                        'stock_inventory.id_executive',
                        'stock_inventory.status_stock',
                        'config_executives.name as name_executive',
                        'config_executives.last_name as last_name_executive',
                        'config_executives.type_stock as type_stock_executive')
                    ->where('stock_inventory.status', '=', '1');
            if($id_user > 0){
                $asesor = Executive::select('id')->where('id_user', '=', $id_user)->get();
                $data = $query->where('stock_inventory.id_executive', '=', $asesor[0]->id)->orderBy('stock_inventory.id', 'desc')->get();
            } else {
                $data = $query->orderBy('stock_inventory.id', 'desc')->get();
            }            

            return response()->json($data);
    
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json(['error' => 'No se encontraron resultados de la consulta.'], 404);
        } catch (\Exception $e) {
            return response()->json($e);
        }
    }

    public function getDetails(Request $request){
        $response = new \stdClass;

        try { 
            $general = StockInventory::
                        join('config_executives', 'config_executives.id', '=', 'stock_inventory.id_executive')
                        ->join('citys', 'citys.id', '=', 'stock_inventory.city')
                        ->join('states', 'states.id', '=', 'stock_inventory.state')
                        ->join('countrys', 'countrys.id', '=', 'stock_inventory.country')
                        ->select(
                            'stock_inventory.id',
                            'stock_inventory.date',
                            'stock_inventory.hour',
                            'stock_inventory.name_receives',
                            'stock_inventory.phone_contact',
                            'stock_inventory.street',
                            'stock_inventory.number_interior',
                            'stock_inventory.number_outdoor',
                            'stock_inventory.suburb',
                            'stock_inventory.postal_code',
                            'stock_inventory.comments',
                            'stock_inventory.subtotal',
                            'stock_inventory.comission',
                            'stock_inventory.total',
                            'citys.name as city',
                            'states.name as state',
                            'countrys.name as country',
                            'stock_inventory.street',
                            'stock_inventory.id_executive',
                            'stock_inventory.status_stock',
                            'stock_inventory.observations',
                            'stock_inventory.amount',
                            'config_executives.name as name_executive',
                            'config_executives.last_name as last_name_executive',
                            'config_executives.type_stock as type_stock_executive')
                        ->where('stock_inventory.status', '=', '1')
                        ->where('stock_inventory.id', '=', $request->input('idStock'))
                        ->get();

                $payments = 
                    StockPayments::
                        join('stock_inventory', 'stock_inventory.id', '=', 'stock_payments.id_stock')
                        ->select('stock_payments.id', 
                                'stock_payments.date_payment',
                                'stock_payments.voucher')
                        ->where('stock_inventory.id', '=', $request->input('idStock'))
                        ->get();
                
                $guides = 
                    StockGuides::
                            join('stock_inventory', 'stock_inventory.id', '=', 'stock_guides.id_stock')
                            ->select('stock_guides.id', 
                                    'stock_guides.guide')
                            ->where('stock_inventory.id', '=', $request->input('idStock'))
                            ->get();

                $equipments = 
                    StockInventoryDetails::
                        join('products_packages', 'products_packages.id', '=', 'stock_inventory_details.id_product')
                        ->select('stock_inventory_details.id', 
                                'products_packages.id as id_equipment',
                                'products_packages.name as equipment',
                                'products_packages.description',
                                'stock_inventory_details.quantity',
                                'stock_inventory_details.price',
                                'stock_inventory_details.comission')
                        ->where('stock_inventory_details.id_stock', '=', $request->input('idStock'))
                        ->get();
            
            $response->general = $general[0];
            $response->payments = $payments;
            $response->guides = $guides;
            $response->equipments = $equipments;
            return response()->json($response);
    
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json(['error' => 'No se encontraron resultados de la consulta.'], 404);
        } catch (\Exception $e) {
            return response()->json($e);
        }
    }
}

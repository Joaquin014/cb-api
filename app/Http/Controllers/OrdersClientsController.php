<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Client;
use App\ClientInvoicing;
use App\Unit;
use App\ClientQuotation;
use App\CreditCollection;
use App\Payments;
use App\SmsSender;
use App\Guide;
use Carbon\Carbon;
use App\Http\Requests\OrdersClientsRequest;
use DB;

class OrdersClientsController extends Controller {

    public function store(OrdersClientsRequest $request){
        try {
            $exist = Client::select('clients.id')->where('clients.phone', '=', $request->input('phone'))->get();  
            
            if(count($exist) > 0){
                $data = ["error" => "Este TELÉFONO ya se encuentra registrado."];
            } else {
                $data = new Client;
                $data->name = $request->input('name');
                $data->last_name_paternal = $request->input('lastNamePaternal');
                $data->last_name_maternal = $request->input('lastNameMaternal');
                $data->email = $request->input('email');
                $data->phone = $request->input('phone');
                $data->country = $request->input('country');
                $data->state = $request->input('state');
                $data->city = $request->input('city');
                $data->postal_code = $request->input('postalCode');
                $data->street = $request->input('street');
                $data->number_interior = $request->input('numberInterior');
                $data->number_outdoor = $request->input('numberOutdoor');
                $data->suburb = $request->input('suburb');
                $data->invoice = $request->input('invoice');
                $data->id_user = $request->input('idUser');
                $data->save();
                $IDC = $data->id;

                if($request->input('invoice') == 1){
                    $clientInv = $request->input('clientInvoicing');
                    $data_ci = new ClientInvoicing;
                    $data_ci->id_client = $IDC;
                    $data_ci->business_name = $clientInv['businessName'];
                    $data_ci->rfc = $clientInv['rfc'];
                    $data_ci->email = $clientInv['emailInvoice'];
                    $data_ci->sfdi = $clientInv['sfdi'];
                    $data_ci->country = $clientInv['country'];
                    $data_ci->state = $clientInv['state'];
                    $data_ci->city = $clientInv['city'];
                    $data_ci->postal_code = $clientInv['postalCode'];
                    $data_ci->street = $clientInv['street'];
                    $data_ci->number_interior = $clientInv['numberInterior'];
                    $data_ci->number_outdoor = $clientInv['numberOutdoor'];
                    $data_ci->suburb = $clientInv['suburb'];
                    $data_ci->id_user = $request->input('idUser');
                    $data_ci->save();
                    $IDCI = $data_ci->id;
                }

                $clientInst = $request->input('clientInstallation');
                $data_u = new Unit;
                $data_u->id_client = $IDC;
                $data_u->plaque = $clientInst['plaque'];
                $data_u->brand = $clientInst['brand'];
                $data_u->model = $clientInst['model'];
                $data_u->color = $clientInst['color'];
                $data_u->serie = $clientInst['serie'];
                $data_u->installation = 1;
                $data_u->country = $clientInst['country'];
                $data_u->state = $clientInst['state'];
                $data_u->city = $clientInst['city'];
                $data_u->postal_code = $clientInst['postalCode'];
                $data_u->street = $clientInst['street'];
                $data_u->number_interior = $clientInst['numberInterior'];
                $data_u->number_outdoor = $clientInst['numberOutdoor'];
                $data_u->suburb = $clientInst['suburb'];
                $data_u->id_user = $request->input('idUser');
                $data_u->save();
                $IDCU = $data_u->id;

                $gpsEquipment = $request->input('gpsEquipment');
                foreach($gpsEquipment as $row){
                    $data_e = new ClientQuotation;
                    $data_e->id_client = $IDC;
                    $data_e->id_equipment = $row['idEquipment'];
                    $data_e->quantity = $row['quantity'];
                    $data_e->id_user = $request->input('idUser');
                    $data_e->save();
                }

                $data_cc = new CreditCollection;
                $data_cc->id_client = $IDC;
                $data_cc->id_company_invoices = 1;
                $data_cc->id_executive = $request->input('idExecutive');
                $data_cc->type_contract = 'COTIZACION';
                $data_cc->period = $request->input('period');
                $data_cc->date_contract = $request->input('date');
                $data_cc->id_user = $request->input('idUser');
                $data_cc->save();

                $sms = new SmsSender;
                $sms->number = "6682534235"; //num Marisol Locmex
                $sms->message = "Pedido nuevo! el cliente ". $request->input('name') . " está en espera de su cotizació0n.";
                $sms->save();

                $sms = new SmsSender;
                $sms->number = $request->input('phone');
                $sms->message = "Hola, excelente día ".$request->input('name'). "! le saluda el departamento de Ventas LOCMEX, su pedido fué recibido con éxito, en breve nos contactaremos. Gracias.";
                $sms->save();
            }

            return response()->json($data);
    
        } catch (\Exception $e) {
            return response()->json($e);
        }
    }

    public function saveVoucher(Request $request){
        try {
            $data = new Payments;
            $data->id_credit_collection = $request->input('idCreditCollection');
            $data->date_payment = Carbon::now();
            $data->month = Carbon::now()->month;
            $data->id_user = $request->input('idUser');
            $data->save();

            return response()->json($data);
    
        } catch (\Exception $e) {
            return response()->json($e);
        }
    }

    public function saveVoucherUrl(Request $request){
        try {
            $data = Payments::findOrFail($request->input('idPayment'));
            $data->voucher = $request->input('voucherUrl');
            $data->save();
            return response()->json($data);
            
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json(['error' => 'No se encontro la solicitud, asegurese de que el id enviado sea correcto.'], 404);
        } catch (\Exception $e) {
            return response()->json($e);
        }
    }

    public function saveGuide(Request $request){
        try {
            $data = new Guide;
            $data->id_credit_collection = $request->input('idCreditCollection');
            $data->id_user = $request->input('idUser');
            $data->save();

            $cc = CreditCollection::findOrFail($request->input('idCreditCollection'));
            $cc->service = "VIGENTE";
            $cc->save();

            return response()->json($data);
    
        } catch (\Exception $e) {
            return response()->json($e);
        }
    }

    public function saveGuideUrl(Request $request){
        try {
            $data = Guide::findOrFail($request->input('idGuide'));
            $data->guide = $request->input('guideUrl');
            $data->save();
            return response()->json($data);
            
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json(['error' => 'No se encontro la solicitud, asegurese de que el id enviado sea correcto.'], 404);
        } catch (\Exception $e) {
            return response()->json($e);
        }
    }
    
    public function saveStatusOrder(Request $request){
        $status = "";
        $number = "";
        $message = "";

        if($request->input('status') == 'PENDIENTE'){
            $status = "EN ESPERA";
        } else if($request->input('status') == 'EN ESPERA'){
            $status = "VERIFICAR";
            $number = "6691430146"; //num Erika Locmex
            $message = "Nuevo pago registrado! hay un cliente en espera de la verificación de su pago.";
        } else if($request->input('status') == 'VERIFICAR'){
            $status = "ENVIAR";
            $number = "6692477766"; //num Carolina Locmex
            $message = "Nuevo envío! hay un cliente en espera del envío de su equipo.";
        } else if($request->input('status') == 'ENVIAR'){
            $status = "VIGENTE";
        }

        try {
            $data = CreditCollection::findOrFail($request->input('idCreditCollection'));
            $data->service = $status;
            $data->save();

            print_r($message);
            print_r($number);

            if($message != '' && $number != ''){
                $sms = new SmsSender;
                $sms->number = $number;
                $sms->message = $message;
                $sms->save();
            }
            
            return response()->json($data);
            
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json(['error' => 'No se encontro la solicitud, asegurese de que el id enviado sea correcto.'], 404);
        } catch (\Exception $e) {
            return response()->json($e);
        }
    }
    
    public function get($id_permission){
        try { 
            $query =  Client::
                        join('credit_collection', 'credit_collection.id_client', '=', 'clients.id')
                        ->join('config_executives', 'config_executives.id', '=', 'credit_collection.id_executive')
                        ->select(
                            'clients.id', 
                            'clients.name', 
                            'clients.last_name_paternal',
                            'clients.last_name_maternal',
                            'config_executives.name AS name_executive',
                            'config_executives.last_name AS last_name_executive',
                            'credit_collection.service AS status_order',
                            DB::raw("DATE_FORMAT(clients.created_at, '%d/%m/%Y') as date"),
                            DB::raw("DATE_FORMAT(clients.created_at, '%H:%i:%s') as hour"),
                            'credit_collection.id as id_credit_collection')
                        ->where('clients.status', '=', 1);
            
            if($id_permission == 13){
                $data = $query->where('credit_collection.service', '=', 'VERIFICAR')->orderBy('credit_collection.id', 'desc')->get();
            } else if($id_permission == 12){
                $data = $query->where('credit_collection.service', '=', 'PENDIENTE')->orWhere('credit_collection.service', '=', 'EN ESPERA')->orderBy('credit_collection.id', 'desc')->get();
            } else if($id_permission == 14){
                $data = $query->where('credit_collection.service', '=', 'ENVIAR')->orderBy('credit_collection.id', 'desc')->get();
            } else {
                $data = $query->orderBy('credit_collection.id', 'desc')->get();
            }
                    
            return response()->json($data);
    
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json(['error' => 'No se encontraron resultados de la consulta.'], 404);
        } catch (\Exception $e) {
            return response()->json($e);
        }
    }

    public function getDetails(Request $request){
        $response = new \stdClass;

        try { 
            $client = 
                Client::
                    join('credit_collection', 'credit_collection.id_client', '=', 'clients.id')
                    ->join('citys', 'citys.id', '=', 'clients.city')
                    ->join('states', 'states.id', '=', 'clients.state')
                    ->join('countrys', 'countrys.id', '=', 'clients.country')
                    ->select('clients.id', 
                            'clients.name', 
                            'clients.last_name_paternal', 
                            'clients.last_name_maternal',
                            'clients.email',
                            'clients.phone',
                            'citys.name as city',
                            'states.name as state',
                            'countrys.name as country',
                            'clients.postal_code',
                            'clients.street',
                            'clients.number_interior',
                            'clients.number_outdoor',
                            'clients.suburb',
                            'clients.invoice',
                            'credit_collection.id as id_credit_collection')
                    ->where('credit_collection.id', '=', $request->input('idCreditCollection'))
                    ->get();

                $payments = 
                    Payments::
                        join('credit_collection', 'credit_collection.id', '=', 'payments.id_credit_collection')
                        ->select('payments.id', 
                                'payments.date_payment', 
                                'payments.month', 
                                'payments.type_payment',
                                'payments.voucher')
                        ->where('credit_collection.id', '=', $request->input('idCreditCollection'))
                        ->get();
                
                $guides = 
                        Guide::
                            join('credit_collection', 'credit_collection.id', '=', 'guides.id_credit_collection')
                            ->select('guides.id', 
                                    'guides.guide')
                            ->where('credit_collection.id', '=', $request->input('idCreditCollection'))
                            ->get();

                $clientInvoicing = 
                    ClientInvoicing::
                        join('citys', 'citys.id', '=', 'clients_invoicing.city')
                        ->join('states', 'states.id', '=', 'clients_invoicing.state')
                        ->join('countrys', 'countrys.id', '=', 'clients_invoicing.country')
                        ->select('clients_invoicing.id', 
                                'clients_invoicing.business_name',
                                'clients_invoicing.rfc',
                                'clients_invoicing.email',
                                'clients_invoicing.sfdi',
                                'citys.name as city',
                                'states.name as state',
                                'countrys.name as country',
                                'clients_invoicing.postal_code',
                                'clients_invoicing.street',
                                'clients_invoicing.number_interior',
                                'clients_invoicing.number_outdoor',
                                'clients_invoicing.suburb')
                        ->where('clients_invoicing.id_client', '=', $client[0]->id)
                        ->get();

                $unit = 
                    Unit::
                        join('citys', 'citys.id', '=', 'clients_units.city')
                        ->join('states', 'states.id', '=', 'clients_units.state')
                        ->join('countrys', 'countrys.id', '=', 'clients_units.country')
                        ->select('clients_units.id', 
                                'clients_units.plaque',
                                'clients_units.brand',
                                'clients_units.model',
                                'clients_units.serie',
                                'clients_units.color',
                                'citys.name as city',
                                'states.name as state',
                                'countrys.name as country',
                                'clients_units.postal_code',
                                'clients_units.street',
                                'clients_units.number_interior',
                                'clients_units.number_outdoor',
                                'clients_units.suburb')
                        ->where('clients_units.id_client', '=', $client[0]->id)
                        ->get();

                $equipments = 
                    ClientQuotation::
                        join('products_packages', 'products_packages.id', '=', 'clients_quotation.id_equipment')
                        ->select('clients_quotation.id', 
                                'products_packages.id as id_equipment',
                                'products_packages.name as equipment',
                                'products_packages.description',
                                'clients_quotation.quantity')
                        ->where('clients_quotation.id_client', '=', $client[0]->id)
                        ->get();
            
            $response->client = $client[0];
            $response->clientInvoicing = $clientInvoicing[0];
            $response->unitInstallation = $unit[0];
            $response->payments = $payments;
            $response->guides = $guides;
            $response->equipments = $equipments;
            return response()->json($response);
    
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json(['error' => 'No se encontraron resultados de la consulta.'], 404);
        } catch (\Exception $e) {
            return response()->json($e);
        }
    }
}

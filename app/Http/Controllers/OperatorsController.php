<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Operators;
use Validator;
use Carbon\Carbon;
use App\Http\Requests\OperatorsRequest;

class OperatorsController extends Controller {
  
  public function store(OperatorsRequest $request){
    try {
        $data = new Operators;
        $data->name = $request->input('name');
        $data->last_name_paternal = $request->input('lastNamePaternal');
        $data->last_name_maternal = $request->input('lastNameMaternal');
        $data->phone = $request->input('phone');
        $data->phone_emergency = $request->input('phoneEmergency');
        $data->nss = $request->input('nss');
        $data->blood_type = $request->input('bloodType');
        $data->license = $request->input('license');
        $data->code = $request->input('code');
        $data->save();
        return response()->json($data);

    } catch (\Exception $e) {
        return response()->json($e);
    }
  }

  public function update(OperatorsRequest $request){

    try {
        $data = Operators::findOrFail($request->input('idOperator'));
        $data->name = $request->input('name');
        $data->last_name_paternal = $request->input('lastNamePaternal');
        $data->last_name_maternal = $request->input('lastNameMaternal');
        $data->phone = $request->input('phone');
        $data->phone_emergency = $request->input('phoneEmergency');
        $data->nss = $request->input('nss');
        $data->blood_type = $request->input('bloodType');
        $data->license = $request->input('license');
        $data->code = $request->input('code');
        $data->save();
        return response()->json($data);
    } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
        return response()->json(['error' => 'No se encontro la solicitud, asegurese de que el id enviado sea correcto.'], 404);
    } catch (\Exception $e) {
        return response()->json($e);
    }
  }

  public function updateImage(OperatorsRequest $request){

    try {
        $data = Operators::findOrFail($request->input('idOperator'));
        $data->image = $request->input('image');
        $data->image_name = $request->input('imageName');
        $data->save();
        return response()->json($data);
    } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
        return response()->json(['error' => 'No se encontro la solicitud, asegurese de que el id enviado sea correcto.'], 404);
    } catch (\Exception $e) {
        return response()->json($e);
    }
  }

  public function get(){
      try { 
        $data = Operators::select(
                  'operators.id', 
                  'operators.name', 
                  'operators.last_name_paternal', 
                  'operators.last_name_maternal', 
                  'operators.phone', 
                  'operators.phone_emergency', 
                  'operators.nss', 
                  'operators.blood_type',
                  'operators.license',
                  'operators.code',
                  'operators.image',
                  'operators.image_name')
            ->where('operators.status', '=', 1)
            ->get();

        return response()->json($data);

    } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
        return response()->json(['error' => 'No se encontraron resultados de la consulta.'], 404);
    } catch (\Exception $e) {
        return response()->json($e);
    }
  }

  public function rem(OperatorsRequest $request){
    try {
        $data = Operators::where('id', '=', $request->input('idOperator'))->update(['status' => 0]);
        
        if($data == 1)
          $data = true;
        else 
          $data = false;
        
        return response()->json($data);

    } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
        return response()->json(['error' => 'No se encontro la solicitud, asegurese de que el id enviado sea correcto.'], 404);
    
    } catch (\Exception $e) {
        return response()->json($e);
    }
  }
}

<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Directions;
use App\OrderLM;
use App\ProcessOrderLM;
use App\DistributionOrderLM;
use App\OrderDetailsLM;
use App\Client;
use Carbon\Carbon;
use App\Http\Requests\OrdersLMRequest;
use DB;

class OrdersLMController extends Controller {

    public function store(Request $request){
        try {

            $idClient = $request->input('idClient');
            $idDirection = $request->input('idDirection');

            if($request->input('capturerDirection') == 1){
                $dir = new Directions;
                $dir->id_type = 4;
                $dir->id_client = $idClient;
                $dir->main_address = '0';
                $dir->country = $request->input('newCountry');
                $dir->state = $request->input('newState');
                $dir->city = $request->input('newCity');
                $dir->postal_code = $request->input('newPostalCode');
                $dir->street = $request->input('newStreet');
                $dir->number_interior = $request->input('newNumberInterior');
                $dir->number_outdoor = $request->input('newNumberOutdoor');
                $dir->suburb = $request->input('newSuburb');
                $dir->save();
                $idDirection = $dir->id;
            }

            if($request->input('idClient') > 0){
                $cli = Client::findOrFail($idClient);
                $cli->name = $request->input('name');
                $cli->last_name_paternal = $request->input('lastNamePaternal');
                $cli->last_name_maternal = $request->input('lastNameMaternal');
                $cli->save();

                $dir = Directions::findOrFail($idDirection);
                $dir->id_type = 4;
                $dir->id_client = $idClient;
                $dir->main_address = '1';
                $dir->country = $request->input('country');
                $dir->state = $request->input('state');
                $dir->city = $request->input('city');
                $dir->postal_code = $request->input('postalCode');
                $dir->street = $request->input('street');
                $dir->number_interior = $request->input('numberInterior');
                $dir->number_outdoor = $request->input('numberOutdoor');
                $dir->suburb = $request->input('suburb');
                $dir->save();
            } else {
                $cli = new Client;
                $cli->id_business = $request->input('idBusiness');
                $cli->name = $request->input('name');
                $cli->last_name_paternal = $request->input('lastNamePaternal');
                $cli->last_name_maternal = $request->input('lastNameMaternal');
                $cli->phone = $request->input('phone');
                $cli->save();
                $idClient = $cli->id;

                $dir = new Directions;
                $dir->id_type = 4;
                $dir->id_client = $idClient;
                $dir->main_address = '1';
                $dir->country = $request->input('country');
                $dir->state = $request->input('state');
                $dir->city = $request->input('city');
                $dir->postal_code = $request->input('postalCode');
                $dir->street = $request->input('street');
                $dir->number_interior = $request->input('numberInterior');
                $dir->number_outdoor = $request->input('numberOutdoor');
                $dir->suburb = $request->input('suburb');
                $dir->save();
                $idDirection = $dir->id;
            }

            $ord = new OrderLM;
            $ord->id_business = $request->input('idBusiness');
            $ord->id_client = $idClient;
            $ord->id_status_order = 2;
            $ord->id_direction = $idDirection;
            $ord->invoice = $request->input('invoice');
            $ord->comments = $request->input('comments');
            $ord->save();

            $products = $request->input('products');
            foreach($products as $row){
                if($row['idProduct'] > 0){
                    $ordDe = new OrderDetailsLM;
                    $ordDe->id_order = $ord->id;
                    $ordDe->id_product = $row['idProduct'];
                    $ordDe->quantity = $row['quantity'];
                    $ordDe->comments = $row['comments'];
                    $ordDe->save();
                }
            }

            $po = new ProcessOrderLM;
            $po->id_order = $ord->id;
            $po->movement = "PENDIENTE";
            $po->date = Carbon::now();
            $po->time = Carbon::now();
            $po->save();

            $po = new ProcessOrderLM;
            $po->id_order = $ord->id;
            $po->movement = "ACEPTADA";
            $po->date = Carbon::now();
            $po->time = Carbon::now();
            $po->save();

            if($request->input('idUnitOperator') > 0){
                $do = new DistributionOrderLM;
                $do->id_order = $ord->id;
                $do->type = "UNIDAD";
                $do->id_assignment = $request->input('idUnitOperator');
                $do->save();
            }

            return response()->json($ord);
    
        } catch (\Exception $e) {
            return response()->json($e);
        }
    }

    public function get(OrdersLMRequest $request){
        try {
            
            $obj = $request->input('obj');

            if($obj == null){
                $orders = OrderLM::join('clients','clients.id','=','orders.id_client')
                    ->join('directions','orders.id_direction','=','directions.id')
                    ->join('countrys','directions.country','=','countrys.id')
                    ->join('states','directions.state','=','states.id')
                    ->join('citys','directions.city','=','citys.id')
                    ->select('orders.id','orders.invoice',
                     DB::raw('CONCAT(clients.name," ",clients.last_name_paternal," ",clients.last_name_maternal) AS name'),
                    'countrys.name AS country','states.name AS state',
                    'citys.name AS city','directions.postal_code as postalCode',
                    'directions.street','directions.number_interior as numberInterior',
                    'directions.number_outdoor as numberOutdoor', 'directions.suburb','orders.status')->get();
            } else {
                $orders = OrderLM::join('clients','clients.id','=','orders.id_client')
                    ->join('directions','orders.id_direction','=','directions.id')
                    ->join('countrys','directions.country','=','countrys.id')
                    ->join('states','directions.state','=','states.id')
                    ->join('citys','directions.city','=','citys.id')
                    ->select('orders.id','orders.invoice',
                    DB::raw('CONCAT(clients.name," ",clients.last_name_paternal," ",clients.last_name_maternal) AS name'),
                    'countrys.name AS country','states.name AS state',
                    'citys.name AS city','directions.postal_code as postalCode',
                    'directions.street','directions.number_interior as numberInterior',
                    'directions.number_outdoor as numberOutdoor', 'directions.suburb','orders.status')
                    ->where('orders.'.$obj["param"], '=',$obj["value"])->get();
            }

            foreach($orders as $item){
                $item->products = OrderLM::
                    join('orders_details', 'orders.id', '=', 'orders_details.id_order')
                    ->join('products', 'orders_details.id_product', '=', 'products.id')
                    ->select(
                    'products.name','orders_details.quantity','products.price_public',
                    DB::raw('(products.price_public * orders_details.quantity) as total'))
                    ->where('orders.invoice', '=',$item->invoice)
                    ->get();
            }

            foreach($orders as $item){
                $item->process = ProcessOrderLM::
                    select(
                        'order_process.date',
                        'order_process.time',
                        'order_process.movement',
                        'order_process.ubication',
                        'order_process.comments')
                    ->where('order_process.id_order', '=', $item->id)
                    ->get();
            }

            foreach($orders as $item){
                $dist = DistributionOrderLM::select('type')->where('id_order', '=', $item->id)->get();
                
                if(count($dist) > 0){
                    if($dist[0]["type"] == 'UNIDAD'){                        
                        $item->unitOperator = DistributionOrderLM::
                            join('operators_units', 'operators_units.id', '=', 'order_distribution.id_assignment')
                            ->join('units', 'units.id', '=', 'operators_units.id_unit')
                            ->join('operators', 'operators.id', '=', 'operators_units.id_operator')
                            ->select(
                                'units.registration as unit',
                                DB::raw('CONCAT(operators.name," ",operators.last_name_paternal," ",operators.last_name_maternal) AS operator'))
                            ->where('order_distribution.id_order', '=', $item->id)
                            ->where('order_distribution.type', '=', 'UNIDAD')
                            ->get();
                    } else {
                        $item->unitOperator = DistributionOrderLM::
                            join('route', 'route.id', '=', 'order_distribution.id_assignment')
                            ->join('operators_units', 'operators_units.id', '=', 'route.id_unit_operator')
                            ->join('units', 'units.id', '=', 'operators_units.id_unit')
                            ->join('operators', 'operators.id', '=', 'operators_units.id_operator')
                            ->select(
                                'order_distribution.type as typeCast',
                                'units.registration as unit',
                                DB::raw('CONCAT(operators.name," ",operators.last_name_paternal," ",operators.last_name_maternal) AS operator'))
                            ->where('order_distribution.id_order', '=', $item->id)
                            ->where('order_distribution.type', '=', 'RUTA')
                            ->get();
                    }
                } else {
                    $item->unitOperator = [];
                }
            }

            return response()->json($orders);
    
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json(['error' => 'No se encontraron resultados de la consulta.'], 404);
        } catch (\Exception $e) {
            return response()->json($e);
        }
    }

    public function getOrdersAsignment(){
        try {
            $orders = OrderLM::
                join('clients','clients.id','=','orders.id_client')
                ->join('directions','orders.id_direction','=','directions.id')
                ->join('countrys','directions.country','=','countrys.id')
                ->join('states','directions.state','=','states.id')
                ->join('citys','directions.city','=','citys.id')
                ->leftjoin('order_distribution','order_distribution.id_order','=','orders.id')
                ->select('orders.id','orders.invoice',
                    DB::raw('CONCAT(clients.name," ",clients.last_name_paternal," ",clients.last_name_maternal) AS name'),
                'countrys.name AS country','states.name AS state',
                'citys.name AS city','directions.postal_code as postalCode',
                'directions.street','directions.number_interior as numberInterior',
                'directions.number_outdoor as numberOutdoor', 'directions.suburb','orders.status')
                ->whereNull('order_distribution.id')->get();
            return response()->json($orders);
    
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json(['error' => 'No se encontraron resultados de la consulta.'], 404);
        } catch (\Exception $e) {
            return response()->json($e);
        }
    }

    public function storeAsignment(Request $request){
        try {
            $do = new DistributionOrderLM;
            $do->id_order = $request->input('idOrder');
            $do->type = "UNIDAD";
            $do->id_assignment = $request->input('idUnitOperator');
            $do->save();
            
            return response()->json($do);
        } catch (\Exception $e) {
            return response()->json($e);
        }
    }
}

<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Intranet;
use App\IntranetDepartment;
use App\IntranetSection;
use App\IntranetUsers;
use DB;

class IntranetController extends Controller
{
    public function getList(){
        $response = new \stdClass;
        
        try {
            $list = Intranet::
                        join('intranet_section', 'intranet_section.id', '=', 'intranet.id_section')            
                        ->join('intranet_department', 'intranet_department.id', '=', 'intranet_section.id_department')
                        ->select(
                            'intranet_department.id as id_department', 
                            'intranet_department.name as name_department',
                            'intranet_section.id as id_section',
                            'intranet_section.name as name_section',
                            'intranet.id',
                            'intranet.bucket',
                            'intranet.binder',
                            'intranet.type',
                            'intranet.archive',
                            'intranet.url')
                        ->where('intranet.status', '=', '1')
                        ->orderBy('intranet.id', 'desc')
                        ->get();

            $binders = Intranet::
                        select(
                            'intranet.id_section', 
                            'intranet.binder')
                        ->where('intranet.status', '=', '1')
                        ->groupBy('intranet.id_section', 'intranet.binder')
                        ->get();

            $response->list = $list;
            $response->binders = $binders;

            return response()->json($response);
    
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json(['error' => 'No se encontraron resultados de la consulta.'], 404);
        } catch (\Exception $e) {
            return response()->json($e);
        }
    }

    public function get($id_user){
        $response = new \stdClass;
        
        try {
            $data = IntranetUsers::
                        join('intranet_department', 'intranet_department.id', '=', 'intranet_users.id_department')
                        ->select('intranet_department.id', 'intranet_department.name')
                        ->where('intranet_users.id_user', '=', $id_user)
                        ->get();
            foreach($data as $row){
                $row->sections = IntranetSection::
                                    select('intranet_section.id', 'intranet_section.name')
                                    ->where('intranet_section.id_department', '=', $row->id)
                                    ->get();
                foreach($row->sections as $row2){
                    $row2->intranet = Intranet::
                                        select(
                                            'intranet.type',
                                            'intranet.bucket',
                                            'intranet.binder',
                                            'intranet.archive',
                                            'intranet.url',
                                            'intranet.id_user',
                                            'intranet.status',
                                            'intranet.created_at')
                                        ->where('intranet.status', '=', '1')
                                        ->where('intranet.id_section', '=', $row2->id)
                                        ->get();
                }
            }

            return response()->json($data);
    
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json(['error' => 'No se encontraron resultados de la consulta.'], 404);
        } catch (\Exception $e) {
            return response()->json($e);
        }
    }

    public function store(Request $request){
        try { 
            
            foreach($request->input('archives') as $row){
                $data = new Intranet;
                $data->id_section = $request->input('idSection');
                $data->type = $row["type"];
                $data->bucket = "drive";
                $data->binder = "drive";
                $data->archive = $row["archive"];
                $data->url = $row["url"];
                $data->id_user = $request->input('idUser');
                $data->save();
            }

            return response()->json($data);
    
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json(['error' => 'No se encontraron resultados de la consulta.'], 404);
        } catch (\Exception $e) {
            return response()->json($e);
        }
    }

    public function saveUrl(Request $request){
        try {
            $data = Intranet::findOrFail($request->input('idIntranet'));
            $data->url = $request->input('archiveUrl');
            $data->save();
            return response()->json($data);
            
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json(['error' => 'No se encontro la solicitud, asegurese de que el id enviado sea correcto.'], 404);
        } catch (\Exception $e) {
            return response()->json($e);
        }
    }

    public function update(Request $request){
        try { 
            $data = Intranet::findOrFail($request->input('idIntranet'));
            $data->id_section = $request->input('idSection');
            $data->type = $request->input('type');
            $data->bucket = $request->input('bucket');
            $data->binder = $request->input('binder');
            $data->archive = $request->input('archive');
            $data->url = $request->input('url');
            $data->id_user = $request->input('idUser');
            $data->save();

            return response()->json($data);
    
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json(['error' => 'No se encontraron resultados de la consulta.'], 404);
        } catch (\Exception $e) {
            return response()->json($e);
        }
    }

    public function delete(Request $request){
        try { 
            $data = Intranet::findOrFail($request->input('idIntranet'));
            $data->status = "0";
            $data->id_user = $request->input('idUser');
            $data->save();

            return response()->json($data);
    
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json(['error' => 'No se encontraron resultados de la consulta.'], 404);
        } catch (\Exception $e) {
            return response()->json($e);
        }
    }
}

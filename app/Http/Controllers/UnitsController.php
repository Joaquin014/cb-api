<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Units;
use App\UnitsOperators;
use Validator;
use Carbon\Carbon;
use App\Http\Requests\UnitsRequest;

class UnitsController extends Controller {
  
  public function store(UnitsRequest $request){
    try {
        $data = new Units;
        $data->id_type_vehicle = $request->input('idTypeVehicle');
        $data->registration = $request->input('registration');
        $data->model = $request->input('model');
        $data->color = $request->input('color');
        $data->brand = $request->input('brand');
        $data->coverage = $request->input('coverage');
        $data->save();

        
        if($request->input('idOperator')){
          $dataOperator = new UnitsOperators;
          $dataOperator->id_unit = $data->id;
          $dataOperator->id_operator = $request->input('idOperator');
          $dataOperator->save();
        }
        
        return response()->json($data);

    } catch (\Exception $e) {
        return response()->json($e);
    }
  }

  public function update(UnitsRequest $request){

    try {
        $data = Units::findOrFail($request->input('idUnit'));
        $data->id_type_vehicle = $request->input('idTypeVehicle');
        $data->registration = $request->input('registration');
        $data->model = $request->input('model');
        $data->color = $request->input('color');
        $data->brand = $request->input('brand');
        $data->coverage = $request->input('coverage');
        $data->save();
        return response()->json($data);
    } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
        return response()->json(['error' => 'No se encontro la solicitud, asegurese de que el id enviado sea correcto.'], 404);
    } catch (\Exception $e) {
        return response()->json($e);
    }
  }

  public function get(){
      try { 
        $data = 
            Units::
            join('type_vehicles', 'units.id_type_vehicle', '=', 'type_vehicles.id')
            ->select(
              'units.id', 
              'units.registration', 
              'units.model', 
              'units.color', 
              'units.brand', 
              'units.coverage',
              'type_vehicles.id as id_type_vehicle',
              'type_vehicles.name as name_vehicle')
            ->where('units.status', '=', 1)
            ->get();

        return response()->json($data);

    } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
        return response()->json(['error' => 'No se encontraron resultados de la consulta.'], 404);
    } catch (\Exception $e) {
        return response()->json($e);
    }
  }

  public function rem(UnitsRequest $request){
    try {
        $data = Units::where('id', '=', $request->input('idUnit'))->update(['status' => 0]);
        
        if($data == 1)
          $data = true;
        else 
          $data = false;
        
        return response()->json($data);

    } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
        return response()->json(['error' => 'No se encontro la solicitud, asegurese de que el id enviado sea correcto.'], 404);
    
    } catch (\Exception $e) {
        return response()->json($e);
    }
  }
}

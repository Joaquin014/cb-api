<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\IndicatorsProspects;
use Validator;
use Carbon\Carbon;
use DB;

class IndicatorsController extends Controller {

    public function get(Request $request){
        $response = new \stdClass;
        $sWhere = "";

        $idExecutiveParam = $request->input('idExecutive');
        if($idExecutiveParam > 0){
            $sWhere = " AND ip.id_executive = $idExecutiveParam";
        }

        try {

            $query = "SELECT 
                        ip.id,
                        ip.date_register,
                        ip.hour_register,
                        CONCAT(DATE_FORMAT(ip.date_cite, '%d/%m/%Y'), ' ', DATE_FORMAT(ip.hour_cite, '%H:%i')) as cite,
                        DATE_FORMAT(ip.date_cite, '%d/%m/%Y') as date_cite,
                        DATE_FORMAT(ip.hour_cite, '%H:%i') as hour_cite,
                        ip.name,
                        ip.phone,
                        ip.email,
                        ip.country,
                        ip.state,
                        ip.city,
                        ip.street,
                        ip.number_outdoor,
                        ip.number_interior,
                        ip.suburb,
                        ip.postal_code,
                        ip.raiting,
                        ip.notes,
                        ip.id_executive
                    FROM indicators_prospects ip
                    WHERE ip.status = '1' $sWhere";
            $data = DB::select($query);

            $response->data = $data;
            return response()->json($response);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json(['error' => 'No se encontraron resultados de la consulta.'], 404);
        } catch (\Exception $e) {
            return response()->json($e);
        } 
    }

    public function store(Request $request){
        try {
            $data = new IndicatorsProspects;
            $data->date_register = $request->input('dateRegister');
            $data->hour_register = $request->input('hourRegister');
            $data->name = $request->input('name');
            $data->phone = $request->input('phone');
            $data->email = $request->input('email');
            $data->country = $request->input('country');
            $data->state = $request->input('state');
            $data->city = $request->input('city');
            $data->street = $request->input('street');
            $data->number_outdoor = $request->input('numberOutdoor');
            $data->number_interior = $request->input('numberInterior');
            $data->suburb = $request->input('suburb');
            $data->postal_code = $request->input('postalCode');
            $data->raiting = $request->input('raiting');
            $data->notes = $request->input('notes');
            $data->id_executive = $request->input('idExecutive');
            $data->id_user = $request->input('idUser');
            if($request->input('cite') === 1){
                $data->date_cite = $request->input('dateCite');
                $data->hour_cite = $request->input('hourCite');
            }

            $data->save();
            return response()->json($data);

        } catch (\Exception $e) {
            return response()->json($e);
        }
    }

    public function update(Request $request){
        try {
            
            $data = IndicatorsProspects::findOrFail($request->input('idProspect'));

            $data->name = $request->input('name');
            $data->phone = $request->input('phone');
            $data->email = $request->input('email');
            $data->country = $request->input('country');
            $data->state = $request->input('state');
            $data->city = $request->input('city');
            $data->street = $request->input('street');
            $data->number_outdoor = $request->input('numberOutdoor');
            $data->number_interior = $request->input('numberInterior');
            $data->suburb = $request->input('suburb');
            $data->postal_code = $request->input('postalCode');
            $data->raiting = $request->input('raiting');
            $data->notes = $request->input('notes');
            $data->id_executive = $request->input('idExecutive');
            $data->id_user = $request->input('idUser');
            if($request->input('cite') === 1){
                $data->date_cite = $request->input('dateCite');
                $data->hour_cite = $request->input('hourCite');
            } else {
                $data->date_cite = null;
                $data->hour_cite = null;
            }
            $data->save();

            return response()->json($data);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json(['error' => 'No se encontro la solicitud, asegurese de que el id enviado sea correcto.'], 404);
        } catch (\Exception $e) {
            return response()->json($e);
        }
    }

    public function updateRaiting(Request $request){
        try {
            
            $data = IndicatorsProspects::findOrFail($request->input('idProspect'));
            $data->raiting = $request->input('raiting');
            $data->id_user = $request->input('idUser');
            $data->save();

            return response()->json($data);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json(['error' => 'No se encontro la solicitud, asegurese de que el id enviado sea correcto.'], 404);
        } catch (\Exception $e) {
            return response()->json($e);
        }
    }

    public function delete(Request $request){
        try {
            
            $data = IndicatorsProspects::findOrFail($request->input('idProspect'));
            $data->status = "0";
            $data->save();

            return response()->json($data);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json(['error' => 'No se encontro la solicitud, asegurese de que el id enviado sea correcto.'], 404);
        } catch (\Exception $e) {
            return response()->json($e);
        }
    }
}

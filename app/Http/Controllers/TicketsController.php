<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Tickets;
use App\TicketsApplicants;
use App\TicketsAnswers;
use App\TicketsHelpType;
use App\IntranetDepartment;
use App\TicketsNotifications;
use App\SmsSender;
use Carbon\Carbon;
use DB;

class TicketsController extends Controller
{
    public function get(){
        $response = new \stdClass;
        
        try {

            $departments = IntranetDepartment::
                        select('intranet_department.id', 'intranet_department.name')
                        ->where('status', '=', '1')
                        ->get();

            $applicants = TicketsApplicants::
                        select('tickets_applicants.id', 'tickets_applicants.name', 'tickets_applicants.last_name','tickets_applicants.id_department', 'tickets_applicants.phone')
                        ->where('status', '=', '1')
                        ->get();

            $helpType = TicketsHelpType::
                        select('tickets_help_type.id', 'tickets_help_type.name')
                        ->where('status', '=', '1')
                        ->get();

            $response->departments = $departments;
            $response->applicants = $applicants;
            $response->helpType = $helpType;
            return response()->json($response);
    
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json(['error' => 'No se encontraron resultados de la consulta.'], 404);
        } catch (\Exception $e) {
            return response()->json($e);
        }
    }

    public function getTickets(Request $request){
        $response = new \stdClass;

        try {

            $status = $request->input('status');

            $query = "SELECT 
                            t.id AS id_ticket,
                            t.priority,
                            idp.id AS id_department,
                            idp.name AS name_department,
                            idp2.id AS id_department_to,
                            idp2.name AS name_department_to,
                            ta.id AS id_applicant,
                            CONCAT(ta.name, ' ', ta.last_name) AS name_applicant,
                            ta2.id AS id_apllicant_to,
                            CONCAT(ta2.name, ' ', ta2.last_name) AS name_applicant_to,
                            hp.id AS id_help_type,
                            hp.name AS name_help_type,
                            DATE_FORMAT(t.date, '%d/%m/%Y') AS `date`,
                            DATE_FORMAT(t.hour, '%H:%i:%s') AS `hour`,
                            DATE_FORMAT(t.date_end, '%d/%m/%Y') AS date_end,
                            DATE_FORMAT(t.hour_end, '%H:%i:%s') AS hour_end,
                            t.subject,
                            t.description,
                            t.ticket_status
                        FROM tickets t
                        INNER JOIN intranet_department idp ON idp.id = t.id_department
                        INNER JOIN intranet_department idp2 ON idp2.id = t.id_department_to
                        INNER JOIN tickets_applicants ta ON ta.id = t.id_applicant
                        INNER JOIN tickets_applicants ta2 ON ta2.id = t.id_applicant_to
                        INNER JOIN tickets_help_type AS hp ON hp.id = t.id_help_type
                        WHERE t.status = 1 AND t.ticket_status = '$status'
                        ORDER BY t.id DESC";
            $tickets = DB::select($query);
            foreach($tickets as $row){
                $query = "SELECT 
                                t.id,
                                t.id_ticket,
                                DATE_FORMAT(t.date, '%d/%m/%Y') AS `date`,
                                DATE_FORMAT(t.hour, '%H:%i:%s') AS `hour`,
                                t.answer,
                                t.comments,
                                p.alias AS username
                            FROM tickets_answers t
                            INNER JOIN users u ON u.id = t.id_user
                            INNER JOIN `profiles` p ON p.id = u.id_profile
                            WHERE t.status = 1 AND t.id_ticket = $row->id_ticket
                            ORDER BY t.id DESC";
                $row->answers = DB::select($query);
            }
            
            $response->tickets = $tickets;
            return response()->json($response);
    
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json(['error' => 'No se encontraron resultados de la consulta.'], 404);
        } catch (\Exception $e) {
            return response()->json($e);
        }
    }

    public function getIndicators(Request $request){
        $response = new \stdClass;
        
        try {

            $query = "SELECT COUNT(1) AS total_tickets FROM tickets WHERE STATUS = '1'";
            $totalTickets = DB::select($query)[0]->total_tickets;
            
            $query = "SELECT COUNT(1) AS open_tickets FROM tickets WHERE STATUS = '1' AND ticket_status = 'ABIERTO'";
            $openTickets = DB::select($query)[0]->open_tickets;

            $query = "SELECT COUNT(1) AS pending_tickets FROM tickets WHERE STATUS = '1' AND ticket_status = 'POR RESOLVER'";
            $pendingTickets = DB::select($query)[0]->pending_tickets; 

            $query = "SELECT COUNT(1) AS closed_tickets FROM tickets WHERE STATUS = '1' AND ticket_status = 'CERRADO'";
            $closedTickets = DB::select($query)[0]->closed_tickets; 

            $query = "SELECT 
                            tickets.id_department, 
                            idp.name AS name_department, 
                            COUNT(1) AS cont 
                        FROM tickets 
                        INNER JOIN intranet_department idp ON idp.id = tickets.id_department
                        WHERE tickets.status = '1' 
                        GROUP BY id_department
                        ORDER BY cont DESC LIMIT 1";
            $starDepartment = DB::select($query); 

            $query = "SELECT 
                            WEEK(created_at) AS semana,
                            COUNT(1) AS tickets 
                        FROM tickets 
                        WHERE tickets.status = '1'
                        GROUP BY semana";
            $graph = DB::select($query); 
            
            $query = "SELECT 
                            tickets.id_department, 
                            idp.name AS name_department, 
                            ta.id AS id_applicant,
                            CONCAT(ta.name, ' ', ta.last_name)AS name_applicant,
                            COUNT(1) AS cont,
                            (SELECT COUNT(1) FROM tickets WHERE id_department = idp.id AND priority = 'EMERGENCIA' AND status = '1') AS emergency,
                            (SELECT COUNT(1) FROM tickets WHERE id_department = idp.id AND priority = 'ALTA' AND status = '1') AS high,
                            (SELECT COUNT(1) FROM tickets WHERE id_department = idp.id AND priority = 'MEDIA' AND status = '1') AS 'medium',
                            (SELECT COUNT(1) FROM tickets WHERE id_department = idp.id AND priority = 'BAJA' AND status = '1') AS 'low',
                            ROUND((COUNT(1) / (SELECT COUNT(1) FROM tickets WHERE STATUS = '1')) * 100, 2) AS porcentage
                        FROM tickets 
                        INNER JOIN intranet_department idp ON idp.id = tickets.id_department
                        INNER JOIN tickets_applicants ta ON ta.id = tickets.id_applicant
                        WHERE tickets.status = '1' -- AND idp.status = '1' AND ta.status = '1'
                        GROUP BY id_department, id_applicant
                        ORDER BY cont DESC";
            $table = DB::select($query);
            
            $query = "SELECT 
                            priority,
                            COUNT(1) AS cont,
                            ROUND(SUM(TIMESTAMPDIFF(HOUR, CONCAT(`date`, ' ', `hour`), CONCAT(`date_end`, ' ', `hour_end`))) / COUNT(1), 2) AS hours
                        FROM tickets
                        WHERE STATUS = '1' AND ticket_status = 'CERRADO'
                        GROUP BY priority
                        ORDER BY priority";
            $avg = DB::select($query);

            $response->totalTickets = $totalTickets;
            $response->openTickets = $openTickets;
            $response->pendingTickets = $pendingTickets;
            $response->closedTickets = $closedTickets;
            $response->starDepartment = $starDepartment;
            $response->emergency = $avg[0];
            $response->high = $avg[1];
            $response->medium = $avg[2];
            $response->low = $avg[3];
            $response->graph = $graph;
            $response->table = $table;
            return response()->json($response);
    
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json(['error' => 'No se encontraron resultados de la consulta.'], 404);
        } catch (\Exception $e) {
            return response()->json($e);
        } 
    }

    public function store(Request $request){
        try { 

            $data = new Tickets;
            $data->id_department = $request->input('idDepartment');
            $data->id_applicant = $request->input('idApplicant');
            $data->id_help_type = $request->input('idHelpType');
            $data->date = Carbon::now();
            $data->hour = Carbon::now();
            $data->priority = $request->input('priority');
            $data->subject = $request->input('subject');
            $data->description = $request->input('description');
            $data->save();

            $nameApplicant = TicketsApplicants::select('name')->where('id', '=', $request->input('idApplicant'))->get()[0]->name;

            $sms = new SmsSender;
            $sms->module = "TICKETS";
            $sms->number = "6692810390"; //num Mariam Locmex
            $sms->message = "Holi Mariam, ". $nameApplicant ." levantó un nuevo Ticket de prioridad ". $request->input('priority') .", ". $request->input('subject') .".";
            $sms->save();

            $sms = new SmsSender;
            $sms->module = "TICKETS";
            $sms->number = "6693334323"; //num Sistemas Locmex
            $sms->message = "Holi Sistemas, ". $nameApplicant ." levantó un nuevo Ticket de prioridad ". $request->input('priority') .", ". $request->input('subject') .".";
            $sms->save();

            $sms = new SmsSender;
            $sms->module = "TICKETS";
            $sms->number = "6691543006"; //num Erick Locmex
            $sms->message = "Holi Erick, ". $nameApplicant ." levantó un nuevo Ticket de prioridad ". $request->input('priority') .", ". $request->input('subject') .".";
            $sms->save();

            $phoneApplicant = TicketsApplicants::select('phone')->where('id', '=', $request->input('idApplicant'))->get()[0]->phone;

            $sms = new SmsSender;
            $sms->module = "TICKETS";
            $sms->number = $phoneApplicant; //num Mariam Locmex
            $sms->message = "Hola ". $nameApplicant .", su número de ticket es ". $data->id.".";
            $sms->save();

            return response()->json($data);
    
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json(['error' => 'No se encontraron resultados de la consulta.'], 404);
        } catch (\Exception $e) {
            return response()->json($e);
        }
    }

    public function answer(Request $request){
        try {

            $data = new TicketsAnswers;
            $data->id_ticket = $request->input('idTicket');
            $data->answer = $request->input('answer');
            $data->comments = $request->input('comments');
            $data->date = $request->input('date');
            $data->hour = $request->input('time');
            $data->id_user = $request->input('idUser');
            $data->save();

            $sms = new SmsSender;
            $sms->module = "TICKETS";
            $sms->number = $request->input('phone');
            $sms->message = $request->input('answer');
            $sms->save();

            return response()->json($data);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json(['error' => 'No se encontro la solicitud, asegurese de que el id enviado sea correcto.'], 404);
        } catch (\Exception $e) {
            return response()->json($e);
        }
    }

    public function postpone(Request $request){
        try {

            $data = Tickets::findOrFail($request->input('idTicket'));
            $data->ticket_status = $request->input('status');
            $data->save();

            return response()->json($data);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json(['error' => 'No se encontro la solicitud, asegurese de que el id enviado sea correcto.'], 404);
        } catch (\Exception $e) {
            return response()->json($e);
        }
    }

    public function close(Request $request){
        try {
            
            $data = Tickets::findOrFail($request->input('idTicket'));
            $data->date_end = $request->input('date');
            $data->hour_end = $request->input('time');
            $data->id_applicant_to = $request->input('applicantTo');
            $data->ticket_status = "CERRADO";
            $data->save();

            $sms = new SmsSender;
            $sms->module = "TICKETS";
            $sms->number = $request->input('phone');
            $sms->message = $request->input('message');
            $sms->save();

            return response()->json($data);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json(['error' => 'No se encontro la solicitud, asegurese de que el id enviado sea correcto.'], 404);
        } catch (\Exception $e) {
            return response()->json($e);
        }
    }
}

<?php
//CLIENTES DE ÚLTIMA MILLA

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Client;
use App\Directions;
use Validator;
use DB;
use App\Http\Requests\ClientRequest;

class ClientController extends Controller {

    public function show($phone) {
        $response = new \stdClass;

        try { 
            $client = 
                Client::
                    join('directions', 'directions.id_client', '=', 'clients.id')
                    ->select(
                        'clients.id', 
                        'clients.id_business',
                        'clients.name',
                        'clients.last_name_paternal as lastNamePaternal',
                        'clients.last_name_maternal as lastNameMaternal',
                        'clients.number_units as numberUnits',
                        'clients.email',
                        'directions.id as idDirection',
                        'directions.country',
                        'directions.state',
                        'directions.city',
                        'directions.postal_code as postalCode',
                        'directions.street',
                        'directions.number_interior as numberInterior',
                        'directions.number_outdoor as numberOutdoor',
                        'directions.suburb',
                        'clients.phone',
                        'clients.invoice')
                    ->where('clients.status', '=', 1)
                    ->where('clients.phone', '=', $phone)
                    ->where('directions.main_address', '=', '1')
                    ->get();

                if(count($client) > 0){
                    $directions =   
                    Directions::
                        join('countrys','directions.country','=','countrys.id')
                        ->join('states','directions.state','=','states.id')
                        ->join('citys','directions.city','=','citys.id')
                        ->select(
                            'directions.id as idDirection',
                            'directions.country',
                            'countrys.name as countryName',
                            'directions.state',
                            'states.name as stateName',
                            'directions.city',
                            'citys.name as cityName',
                            'directions.postal_code as postalCode',
                            'directions.street',
                            'directions.number_interior as numberInterior',
                            'directions.number_outdoor as numberOutdoor',
                            'directions.suburb',
                            'directions.status')
                        ->where('directions.id_client', '=', $client[0]->id)
                    ->where('directions.status', '=', '1')
                    ->orderBy('directions.main_address', 'asc')
                    ->get();

                    $response->client = $client[0];
                    $response->directions = $directions;
                } else {
                    $response = ["error" => "No se encontro ningún cliente registrado."];
                }
    
            return response()->json($response);
    
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json(['error' => 'No se encontraron resultados de la consulta.'], 404);
        } catch (\Exception $e) {
            return response()->json($e);
        }
    }

    public function updateDir(Request $request) {
        try {
            $d = Directions::where('id_client', $request->input('idClient'))->update(['main_address' => '0']);
            $d = Directions::where('id', $request->input('idDirection'))->update(['main_address' => '1']);
            
            return response()->json($d);
        } catch (\Exception $e) {
            return response()->json($e);
        }
       
    }

    public function create() {}

    public function store(ClientRequest $request) {}

    public function edit($id) {}

    public function update(ClientRequest $request, $id) {

    }
    public function getClient($id){
        
    }

    public function destroy($id) {}
}

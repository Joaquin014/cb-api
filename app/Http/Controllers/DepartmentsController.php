<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\IntranetDepartment;
use DB;

class DepartmentsController extends Controller
{

    public function get(){
        try { 
            $data = IntranetDepartment::
                      select(
                        'intranet_department.id',
                        'intranet_department.name',
                        'intranet_department.mandated')
                      ->where('intranet_department.status', '=', '1')
                      ->orderBy('intranet_department.name', 'asc')
                      ->get();
                    
            return response()->json($data);
    
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json(['error' => 'No se encontraron resultados de la consulta.'], 404);
        } catch (\Exception $e) {
            return response()->json($e);
        }
    }
    
    public function store(Request $request){
        try {
            $data = new IntranetDepartment;
            $data->name = $request->input('name');
            $data->mandated = $request->input('mandated');
            $data->id_user = $request->input('idUser');
            $data->save();

            return response()->json($data);
    
        } catch (\Exception $e) {
            return response()->json($e);
        }
    }

    public function update(Request $request){
        try {
            $data = IntranetDepartment::findOrFail($request->input('idDepartment'));
            $data->name = $request->input('name');
            $data->mandated = $request->input('mandated');
            $data->id_user = $request->input('idUser');
            $data->save();
            return response()->json($data);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json(['error' => 'No se encontro la solicitud, asegurese de que el id enviado sea correcto.'], 404);
        } catch (\Exception $e) {
            return response()->json($e);
        }
    }
    
    public function delete(Request $request){
        try {
            $data = IntranetDepartment::findOrFail($request->input('idDepartment'));
            $data->status = '0';
            $data->id_user = $request->input('idUser');
            $data->save();
            return response()->json($data);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json(['error' => 'No se encontro la solicitud, asegurese de que el id enviado sea correcto.'], 404);
        } catch (\Exception $e) {
            return response()->json($e);
        }
    }
}

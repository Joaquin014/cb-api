<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Log;

use DB;

class LogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $all = Log::select('descripcion', 'id', 'timestamp', 'tipo', 'tabla')->orderBy("id", "DESC")->get();
        return response()->json($all);        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($evento, $id_usuario, $tabla, $id_reg, $values)
    {
        //
	    $usuario = 'Prueba';
        $n_evento = 'creó';
        if($evento == 'edit'){
            $n_evento = 'modificó';
        } else if($evento == 'delete'){
            $n_evento = 'eliminó';
        }

	    $descripcion = 'Usuario: '.$usuario.' '.$n_evento.' el registro ID: '.$id_reg.'; '.$values;

        $data = new Log;
        $data->descripcion = $descripcion;
        $data->tabla  = $tabla;
        $data->tipo  = $evento;
        $data->id_registro  = $id_reg;
        $data->id_usuario  = $id_usuario;
        $data->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

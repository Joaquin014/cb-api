<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\TypeStatusOrders;
use Validator;
use Carbon\Carbon;
use App\Http\Requests\TypeStatusOrdersRequest;

class TypeStatusOrdersController extends Controller {
  
  public function store(TypeStatusOrdersRequest $request){
    try {
        $data = new TypeStatusOrders;
        $data->name = $request->input('name');
        $data->save();
        return response()->json($data);

    } catch (\Exception $e) {
        return response()->json($e);
    }
  }

  public function update(TypeStatusOrdersRequest $request){

    try {
        $data = TypeStatusOrders::findOrFail($request->input('idTypeStatusOrder'));
        $data->name = $request->input('name');
        $data->save();
        return response()->json($data);
    } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
        return response()->json(['error' => 'No se encontro la solicitud, asegurese de que el id enviado sea correcto.'], 404);
    } catch (\Exception $e) {
        return response()->json($e);
    }
  }

  public function get(){
      try { 
        $data = TypeStatusOrders::select('type_status_orders.id', 'type_status_orders.name')
            ->where('type_status_orders.status', '=', 1)
            ->get();

        return response()->json($data);

    } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
        return response()->json(['error' => 'No se encontraron resultados de la consulta.'], 404);
    } catch (\Exception $e) {
        return response()->json($e);
    }
  }

  public function rem(TypeStatusOrdersRequest $request){
    try {
        $data = TypeStatusOrders::where('id', '=', $request->input('idTypeStatusOrder'))->update(['status' => 0]);
        
        if($data == 1)
          $data = true;
        else 
          $data = false;
        
        return response()->json($data);

    } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
        return response()->json(['error' => 'No se encontro la solicitud, asegurese de que el id enviado sea correcto.'], 404);
    
    } catch (\Exception $e) {
        return response()->json($e);
    }
  }
}

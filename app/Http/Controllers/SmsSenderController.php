<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\SmsSender;
use Carbon\Carbon;
use DB;

class SmsSenderController extends Controller {
    public function store(Request $request){
        try {
          $sms = new SmsSender;
          $sms->number = $request->input('phone');
          $sms->message = $request->input('message');
          $sms->save();

          return response()->json($sms);
        } catch (\Exception $e) {
          return response()->json($e);
        }
    }
}

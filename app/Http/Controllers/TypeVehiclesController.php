<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\TypeVehicles;
use Validator;
use Carbon\Carbon;
use App\Http\Requests\TypeVehiclesRequest;

class TypeVehiclesController extends Controller {
  
  public function store(TypeVehiclesRequest $request){
    try {
        $data = new TypeVehicles;
        $data->name = $request->input('name');
        $data->save();
        return response()->json($data);

    } catch (\Exception $e) {
        return response()->json($e);
    }
  }

  public function update(TypeVehiclesRequest $request){

    try {
        $data = TypeVehicles::findOrFail($request->input('idTypeVehicle'));
        $data->name = $request->input('name');
        $data->save();
        return response()->json($data);
    } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
        return response()->json(['error' => 'No se encontro la solicitud, asegurese de que el id enviado sea correcto.'], 404);
    } catch (\Exception $e) {
        return response()->json($e);
    }
  }

  public function get(){
      try { 
        $data = TypeVehicles::select('type_vehicles.id', 'type_vehicles.name')
            ->where('type_vehicles.status', '=', 1)
            ->get();

        return response()->json($data);

    } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
        return response()->json(['error' => 'No se encontraron resultados de la consulta.'], 404);
    } catch (\Exception $e) {
        return response()->json($e);
    }
  }

  public function rem(TypeVehiclesRequest $request){
    try {
        $data = TypeVehicles::where('id', '=', $request->input('idTypeVehicle'))->update(['status' => 0]);
        
        if($data == 1)
          $data = true;
        else 
          $data = false;
        
        return response()->json($data);

    } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
        return response()->json(['error' => 'No se encontro la solicitud, asegurese de que el id enviado sea correcto.'], 404);
    
    } catch (\Exception $e) {
        return response()->json($e);
    }
  }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Executive;
use Validator;
use Carbon\Carbon;
use App\Http\Requests\ExecutivesRequest;
use DB;

class ExecutivesController extends Controller {
  
  public function get($id_user){
      try { 
        $r = Executive::select(DB::raw('count(*) as count'))->where('config_executives.status', '=', 1)->where('id_user', '=', $id_user)->get();

        $query = 
          Executive::
            select(
              'config_executives.id', 
              'config_executives.name', 
              'config_executives.last_name',
              'config_executives.id_user',
              'config_executives.type_stock')
            ->where('config_executives.status', '=', 1);
        
        if($r[0]->count > 0){
          $query = $query->where('config_executives.id_user', '=', $id_user);
        }

        $data = $query->get();

        return response()->json($data);

    } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
        return response()->json(['error' => 'No se encontraron resultados de la consulta.'], 404);
    } catch (\Exception $e) {
        return response()->json($e);
    }
  }
}

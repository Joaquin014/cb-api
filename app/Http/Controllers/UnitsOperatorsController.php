<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Units;
use App\Operators;
use App\UnitsOperators;
use Validator;
use Carbon\Carbon;
use App\Http\Requests\UnitOperatorsRequest;
use DB;

class UnitsOperatorsController extends Controller {
  
  public function store(UnitOperatorsRequest $request){
    try {

          $data = 
          UnitsOperators::
            select('id')
            ->where('id_operator', '=', $request->input('idOperator'))
            ->where('id_unit', '=', $request->input('idUnit'))
            ->get();

          if(count($data) > 0){

            $data = ["error" => "Esta asignación ya éxiste"];

          } else {
            $data = new UnitsOperators;
            $data->id_unit = $request->input('idUnit');
            $data->id_operator = $request->input('idOperator');
            $data->save();
          }
          
          return response()->json($data);

    } catch (\Exception $e) {
        return response()->json($e);
    }
  }

  public function update(UnitOperatorsRequest $request){

    try {
          $data = UnitsOperators::findOrFail($request->input('idAssigment'));
          $data->id_unit = $request->input('idUnit');
          $data->id_operator = $request->input('idOperator');
          $data->save();

          return response()->json($data);

    } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
        return response()->json(['error' => 'No se encontro la solicitud, asegurese de que el id enviado sea correcto.'], 404);
    } catch (\Exception $e) {
        return response()->json($e);
    }
  }

  public function get($id_business){
      try { 
        $data = 
            UnitsOperators::
            join('operators', 'operators.id', '=', 'operators_units.id_operator')
            ->join('units', 'units.id', '=', 'operators_units.id_unit')
            ->select(
              'operators_units.id', 
              DB::raw("CONCAT('MATRICULA : ', units.registration) AS unit"),
              DB::raw("CONCAT(operators.name, ' ', operators.last_name_paternal) AS operator"),
              'operators.id as id_operator',
              'units.id as id_unit')
            ->where('operators_units.id_business', '=', $id_business)
            ->where('operators_units.status', '=', 1)
            ->get();

        return response()->json($data);

    } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
        return response()->json(['error' => 'No se encontraron resultados de la consulta.'], 404);
    } catch (\Exception $e) {
        return response()->json($e);
    }
  }

  public function rem(UnitOperatorsRequest $request){
    try {

          $data = UnitsOperators::where('id', '=', $request->input('idAssigment'))->update(['status' => 0]);
        
          if($data == 1)
            $data = true;
          else 
            $data = false;
          
          return response()->json($data);

    } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
        return response()->json(['error' => 'No se encontro la solicitud, asegurese de que el id enviado sea correcto.'], 404);
    
    } catch (\Exception $e) {
        return response()->json($e);
    }
  }
}

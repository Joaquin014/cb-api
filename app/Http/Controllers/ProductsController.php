<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\ProductsPackages;
use App\Products;
use Validator;
use Carbon\Carbon;
use App\Http\Requests\ProductsRequest;

class ProductsController extends Controller {
  
  public function getPackages(){
      try { 
        $data = 
          ProductsPackages::select(
            'products_packages.id', 
            'products_packages.period',
            'products_packages.name', 
            'products_packages.description',
            'products_packages.price',
            'products_packages.montly_payment')
          ->where('products_packages.status', '=', 1)
          ->get();

        return response()->json($data);

    } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
        return response()->json(['error' => 'No se encontraron resultados de la consulta.'], 404);
    } catch (\Exception $e) {
        return response()->json($e);
    }
  }

  public function getProducts($id_business){
      try { 
        $data = 
          Products::select(
            'products.id',  
            'products.name', 
            'products.identity',
            'products.price',
            'products.price_public')
          ->where('products.id_business', '=', $id_business)
          ->where('products.status', '=', 1)
          ->get();

        return response()->json($data);

    } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
        return response()->json(['error' => 'No se encontraron resultados de la consulta.'], 404);
    } catch (\Exception $e) {
        return response()->json($e);
    }
  }
}

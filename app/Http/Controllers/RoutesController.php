<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Route;
use Validator;
use Carbon\Carbon;
use App\Http\Requests\RoutesRequest;

class RoutesController extends Controller {
  
  public function storeRoute(RoutesRequest $request){
    try {
        $data = new Route;
        $data->name = $request->input('name');
        $data->id_unit_operator = $request->input('idAssignment');
        $data->characteristic = $request->input('characteristic');
        $data->save();
        return response()->json($data);

    } catch (\Exception $e) {
        return response()->json($e);
    }
  }

  public function updateRoute(RoutesRequest $request){

    try {
        $data = Route::findOrFail($request->input('idRoute'));
        $data->name = $request->input('name');
        $data->id_unit_operator = $request->input('idAssignment');
        $data->characteristic = $request->input('characteristic');
        $data->save();
        return response()->json($data);
    } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
        return response()->json(['error' => 'No se encontro la solicitud, asegurese de que el id enviado sea correcto.'], 404);
    } catch (\Exception $e) {
        return response()->json($e);
    }
  }

  public function getRoute(){
      try { 
        $route = Route::
          join('operators_units', 'operators_units.id', '=', 'route.id_unit_operator')  
          ->join('operators', 'operators.id', '=', 'operators_units.id_operator')
          ->join('units', 'units.id', '=', 'operators_units.id_unit')
          ->select(
            'route.id', 
            'route.name', 
            'route.characteristic',
            'operators.name as name_operator',
            'units.registration as name_unit',
            'route.id_unit_operator as idAssignment')
            ->where('route.status', '=', 1)
            ->get();

        return response()->json($route);

    } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
        return response()->json(['error' => 'No se encontraron resultados de la consulta.'], 404);
    } catch (\Exception $e) {
        return response()->json($e);
    }
  }

  public function remRoute(RoutesRequest $request){
    try {
        $route = Route::where('id', '=', $request->input('idRoute'))->update(['status' => 0]);
        
        if($route == 1)
          $route = true;
        else 
          $route = false;
        
        return response()->json($route);

    } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
        return response()->json(['error' => 'No se encontro la solicitud, asegurese de que el id enviado sea correcto.'], 404);
    
    } catch (\Exception $e) {
        return response()->json($e);
    }
  }
}

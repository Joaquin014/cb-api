<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\User;
use App\Token;
use Validator;
use Carbon\Carbon;
use App\Http\Requests\LogInRequest;

class LogInController extends Controller {

  public function logIn(LogInRequest $request) {
    $user = [];
    $user['info'] = ['status' => 200];

    try { 
      $user['data'] = User::
          join('profiles', 'users.id_profile', '=', 'profiles.id')
          ->join('permissions', 'users.id_permission', '=', 'permissions.id')
          ->join('modulated', 'modulated.id', '=', 'permissions.id_modulated')
          ->join('type_platform', 'users.id_platform', '=', 'type_platform.id')
          ->join('business', 'business.id', '=', 'users.id_business')
          ->select(
              'users.id',
              'users.user',
              'business.id AS id_business',
              'business.business_name',
              'business.alias AS business_alias',
              'type_platform.id as id_platform',
              'type_platform.name as name_platform',
              'profiles.id as id_profile',
              'profiles.name',
              'profiles.last_name_paternal',
              'profiles.last_name_maternal', 
              'profiles.alias', 
              'profiles.date_birth', 
              'profiles.email',
              'profiles.phone',
              'profiles.sex',
              'profiles.image',
              'profiles.theme',
              'profiles.menu_bar',
              'profiles.created_at',
              'profiles.updated_at',
              'permissions.id as type_permission',
              'permissions.name as name_permission',
              'permissions.p_select',
              'permissions.p_create',
              'permissions.p_update',
              'permissions.p_delete',
              'modulated.name as modulated_name',
              'modulated.id as modulated_id')
          ->where('users.user', '=', $request->input('user'))
          ->where('users.password', '=', SHA1($request->input('password')))
          ->whereIn('users.id_platform', [1, $request->input('typePlatform')])
          ->get();

          if( count($user['data']) == 0 ) $user['info'] = ['error' => 'No hay registros disponibles con ese ID.', 'status' => 404];
          
        return response()->json($user);

    } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
        return response()->json(['error' => 'No se encontraron resultados de la consulta.'], 404);
    } catch (\Exception $e) {
        return response()->json($e);
    }
  }
  
  public function setToken(Request $request){
    try {
        $data = new Token;
        $data->id_user = $request->input('idUser');
        $data->token = SHA1($request->input('idUser') + Carbon::now()->timestamp + $request->input('idPermission') + $request->input('idPlatform'));
        $data->expiration = Carbon::now()->addMonths(1);
        $data->save();
        return response()->json($data);

    } catch (\Exception $e) {
        return response()->json($e);
    }
  }

  public function getToken(Request $request){
      try { 
        $token = Token::
            select('users_tokens.token')
            ->where('users_tokens.token', '=', $request->input('token'))
            ->where('users_tokens.status', '=', 1)
            ->where('users_tokens.expiration', '>=', Carbon::now())
            ->get();
        
        if(count($token) > 0)
          $token = true;
        else 
          $token = false;
        
        return response()->json($token);

    } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
        return response()->json(['error' => 'No se encontraron resultados de la consulta.'], 404);
    } catch (\Exception $e) {
        return response()->json($e);
    }
  }

  public function remToken(Request $request){
    try {
        $token = Token::where('token', '=', $request->input('token'))->update(['status' => 0]);
        
        if($token == 1)
          $token = true;
        else 
          $token = false;
        
        return response()->json($token);

    } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
        return response()->json(['error' => 'No se encontro la solicitud, asegurese de que el id enviado sea correcto.'], 404);
    
    } catch (\Exception $e) {
        return response()->json($e);
    }
  }
}

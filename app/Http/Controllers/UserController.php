<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\UserM;
use App\Profile;

use DB;

class UserController extends Controller
{

    public function get_login(Request $request)
    {
        //
        $correo = $request->input('correo');
        $contra  = $request->input('contrasena');

        $user = UserM::select("id", "nombres", "apellido_paterno", "apellido_materno", "contacto", "correo", "usuario", "fecha_nacimiento","alias")
        ->where('usuario', $correo)
        ->where('contrasena', $contra)
        ->where('estado', 1)
        ->get();
        
        $response = sizeof($user) == 0 ? array() : $user[0];
        return response()->json($response);        
    }

    public function setTheme(Request $request) {
        try {
            $data = Profile::findOrFail($request->input('idProfile'));
            $data->theme = $request->input('theme');
            $data->save();
            
            return response()->json($data);

        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json(['error' => 'No se encontro la solicitud, asegurese de que el id enviado sea correcto.'], 404);
        
        } catch (\Exception $e) {
            return response()->json($e);
        }       
    }

    public function getTheme($idProfile) {
        $theme = Profile::select("theme")
        ->where('id', $idProfile)
        ->get();
        
        if( count($theme) == 0 ) $theme[0] = ['error' => 'No hay registros disponibles con ese ID.', 'status' => 404];
        return response()->json($theme[0]); 
    }
}

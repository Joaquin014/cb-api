<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class OperatorsRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //obtener el URI desde donde se esta instanciando el objeto y dividirlo en palabras individuales en cada /
        $method = explode('/',$this::path());
        
        //selecionar el ultimo elemento de la ruta 'unitOperator/store' y crear la regla dependiendo el caso
        switch($method[count($method)-1]){
            case 'store':
                $rules = [
                    'name' => 'required',
                    'lastNamePaternal' => 'required',
                    'lastNameMaternal' => 'required',
                    'bloodType' => 'required',
                    'phone' => 'required',
                    'phoneEmergency' => 'required',
                    'nss' => 'required',
                    'license' => 'required',
                    'code' => 'required'
                ];
                break;
            case 'update':
                $rules = [
                    'idOperator' => 'required|integer',
                    'name' => 'required',
                    'lastNamePaternal' => 'required',
                    'lastNameMaternal' => 'required',
                    'bloodType' => 'required',
                    'phone' => 'required',
                    'phoneEmergency' => 'required',
                    'nss' => 'required',
                    'license' => 'required',
                    'code' => 'required'
                ];
                break;
            case 'updateImage':
                $rules = [
                    'idOperator' => 'required|integer',
                    'image' => 'required',
                    'imageName' => 'required'
                ];
                break;
            case 'rem':
                $rules = [
                    'idOperator' => 'required|integer'
                ];
                break;
        }
        return $rules;
    }

    public function attributes()
    {
        $method = explode('/',$this::path());
        switch($method[count($method)-1]){
            case 'store':
                $attributes = [
                    'name' => 'nombre de tipo de status'
                ];
                break;
            case 'update':
                $attributes = [
                    'idTypeStatusOrder' => 'id del tipo de status',
                    'name' => 'nombre de tipo de status'
                ];
                break;
            case 'rem':
                $attributes = [
                    'idTypeStatusOrder' => 'id del tipo de status'
                ];
                break;
        }
        return $attributes;
    }

    public function response(array $errors)
    {
        return response()->json($errors, 422);
    }
}
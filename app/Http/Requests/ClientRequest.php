<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ClientRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // if( $this->method() === "PUT")
        return [
            'nombre' => 'required|string|max:255',
            'apellido' => 'required|string|max:255',
            'tipo' => 'integer|min:1|max:100',
            'estado' => 'boolean',
        ];
    }

    public function attributes()
    {
        return [
            'nombre' => 'nombre del cliente',
            'apellido' => 'apellido del cliente',
            'tipo' => 'tipo de cliente',
            'estado' => 'estado del cliente'
      ];
   }

    public function response(array $errors)
    {
        return response()->json($errors, 422);
    }
}

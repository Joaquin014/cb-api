<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UnitsRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //obtener el URI desde donde se esta instanciando el objeto y dividirlo en palabras individuales en cada /
        $method = explode('/',$this::path());
        
        //selecionar el ultimo elemento de la ruta 'unitOperator/store' y crear la regla dependiendo el caso
        switch($method[count($method)-1]){
            case 'store':
                $rules = [
                    'idTypeVehicle' => 'required|integer',
                    'registration' => 'required',
                    'model' => 'required',
                    'color' => 'required',
                    'brand' => 'required',
                    'coverage' => 'required',
                    'idOperator' => 'required|integer'
                ];
                break;
            case 'update':
                $rules = [
                    'idUnit' => 'required|integer',
                    'idTypeVehicle' => 'required|integer',
                    'registration' => 'required',
                    'model' => 'required',
                    'color' => 'required',
                    'brand' => 'required',
                    'coverage' => 'required'
                ];
                break;
            case 'rem':
                $rules = [
                    'idUnit' => 'required|integer',
                ];
                break;
        }
        return $rules;
    }

    public function attributes()
    {
        $method = explode('/',$this::path());
        switch($method[count($method)-1]){
            case 'store':
                $attributes = [
                    'idTypeVehicle' => 'id tipo de vehiculo',
                    'registration' => 'registro',
                    'model' => 'modelo',
                    'color' => 'color',
                    'brand' => 'marca',
                    'coverage' => 'cobertura',
                    'idOperator' => 'id del operador'
                ];
                break;
            case 'update':
                $attributes = [
                    'idUnit' => 'id de la unidad',
                    'idTypeVehicle' => 'id tipo de vehiculo',
                    'registration' => 'registro',
                    'model' => 'modelo',
                    'color' => 'color',
                    'brand' => 'marca',
                    'coverage' => 'cobertura'
                ];
                break;
            case 'rem':
                $attributes = [
                    'idUnit' => 'id de la unidad',
                ];
                break;
        }
        return $attributes;
    }

    public function response(array $errors)
    {
        return response()->json($errors, 422);
    }
}
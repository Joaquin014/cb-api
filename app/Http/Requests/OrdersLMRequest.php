<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class OrdersLMRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //obtener el URI desde donde se esta instanciando el objeto y dividirlo en palabras individuales en cada /
        $method = explode('/',$this::path());
        
        //selecionar el ultimo elemento de la ruta 'unitOperator/store' y crear la regla dependiendo el caso
        switch($method[count($method)-1]){
            case 'store':
                $rules = [
                    'idUser' => 'required|integer',
                    'invoice' => 'required|integer',
                    'products.*.idProduct' => 'required|integer',
                    'products.*.quantity' => 'required|integer',
                    'products.*.comments' => 'sometimes|required'
                ];
                break;
            case 'update':
                $rules = [];
            case 'rem':
                $rules = [];
                break;
            case 'get':
                $rules = [
                    'obj' => 'sometimes|required',
                    'obj.param' => 'required_with:obj',
                    'obj.value' => 'required_with:obj'
                ];
                break;
        }
        return $rules;
    }

    public function attributes()
    {
        $method = explode('/',$this::path());
        switch($method[count($method)-1]){
            case 'store':
                $attributes = [
                    'idUser' => 'id del usuario',
                    'invoice' => 'folio',
                    'products.*.idProduct' => 'id del producto',
                    'products.*.quantity' => 'cantidad de producto',
                    'products.*.comments' => 'comentarios'
                ];
            case 'update':
                $attributes = [];
            case 'rem':
                $attributes = [];
                break;
            case 'get':
                $attributes = [
                    'obj' =>'objeto para consulta',
                    'obj.param' => 'parametro de consulta',
                    'obj.value' => 'valor del parametro'
                ];
                break;
        }
        return $attributes;
    }

    public function response(array $errors)
    {
        return response()->json($errors, 422);
    }
}
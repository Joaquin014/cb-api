<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UnitOperatorsRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //obtener el URI desde donde se esta instanciando el objeto y dividirlo en palabras individuales en cada /
        $method = explode('/',$this::path());
        
        //selecionar el ultimo elemento de la ruta 'unitOperator/store' y crear la regla dependiendo el caso
        switch($method[count($method)-1]){
            case 'store':
                $rules = [
                    'idOperator' => 'required|integer',
                    'idUnit' => 'required|integer'
                ];
                break;
            case 'update':
                $rules = [
                    'idAssigment' => 'required|integer',
                    'idOperator' => 'required|integer',
                    'idUnit' => 'required|integer'
                ];
                break;
            case 'rem':
                $rules = [
                    'idAssigment' => 'required|integer',
                ];
                break;
        }
        return $rules;
    }

    public function attributes()
    {
        $method = explode('/',$this::path());
        switch($method[count($method)-1]){
            case 'store':
                $attributes = [
                    'idOperator' => 'id del operador',
                    'idUnit' => 'id de la unidad'
                ];
                break;
            case 'update':
                $attributes = [
                    'idAssigment' => 'id de asignacion',
                    'idOperator' => 'id del operador',
                    'idUnit' => 'id de la unidad'
                ];
                break;
            case 'rem':
                $attributes = [
                    'idAssigment' => 'id de la asignacion',
                ];
                break;
        }
        return $attributes;
    }

    public function response(array $errors)
    {
        return response()->json($errors, 422);
    }
}
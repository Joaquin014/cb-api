<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class LogInRequest extends Request
{

    public function authorize() {
        return true;
    }

    public function rules() {
      return [
        'user' => 'required',
        'password' => 'required',
      ];
    }

    public function attributes() {
      return [
        'usuario' => 'nombre del cliente',
        'contraseña' => 'apellido del cliente',
      ];
   }

    public function response(array $errors) {
      return response()->json($errors, 422);
    }
}

<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class OrdersClientsRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //obtener el URI desde donde se esta instanciando el objeto y dividirlo en palabras individuales en cada /
        $method = explode('/',$this::path());
        
        //selecionar el ultimo elemento de la ruta 'unitOperator/store' y crear la regla dependiendo el caso
        switch($method[count($method)-1]){
            case 'store':
                $rules = [
                    /* 'name' => 'required',
                    'lastNamePaternal' => 'required',
                    'lastNameMaternal' => 'required',
                    'email' => 'required',
                    'phone' => 'required',
                    'country' => 'required|integer',
                    'state' => 'required|integer',
                    'city'=> 'required|integer',
                    'postalCode' => 'required|integer',
                    'numberInterior' => 'required|integer',
                    'numberOutdoor' => 'required|integer',
                    'street' => 'required',
                    'suburb' => 'required',
                    'idUser' => 'required|integer',

                    'clientInvoicing.businessName' => 'required',
                    'clientInvoicing.rfc' => 'required',
                    'clientInvoicing.country' => 'required|integer',
                    'clientInvoicing.state' => 'required|integer',
                    'clientInvoicing.city'=> 'required|integer',
                    'clientInvoicing.postalCode' => 'required|integer',
                    'clientInvoicing.numberInterior' => 'required|integer',
                    'clientInvoicing.numberOutdoor' => 'required|integer',
                    'clientInvoicing.street' => 'required',
                    'clientInvoicing.suburb' => 'required',

                    'plaque' => 'required',
                    'model' => 'required',
                    'brand' => 'required',
                    'serie' => 'required',
                    'color' => 'required',

                    'clientInstallation.country' => 'required|integer',
                    'clientInstallation.state' => 'required|integer',
                    'clientInstallation.city'=> 'required|integer',
                    'clientInstallation.postalCode' => 'required|integer',
                    'clientInstallation.numberInterior' => 'required|integer',
                    'clientInstallation.numberOutdoor' => 'required|integer',
                    'clientInstallation.street' => 'required',
                    'clientInstallation.suburb' => 'required',

                    'gpsEquipment.*.idEquipment' => 'required|integer',
                    'gpsEquipment.*.quantity' => 'required|integer',

                    'idExecutive' => 'required|integer',
                    'period' => 'required',
                    'date' => 'required' */
                ];
                break;
        }
        return $rules;
    }

    public function attributes()
    {
        $method = explode('/',$this::path());
        switch($method[count($method)-1]){
            case 'store':
                $attributes = [
                    'name' => 'nombre del cliente',
                    'lastNamePaternal' => 'apellido paterno',
                    'lastNameMaternal' => 'apellido materno',
                    'email' => 'correo electronico',
                    'phone' => 'telefono',
                    'country' => 'pais',
                    'state' => 'estado',
                    'city '=> 'ciudad',
                    'postalCode' => 'codigo postal',
                    'numberInterior' => 'numero interior',
                    'numberOutdoor' => 'numero exterior',
                    'street' => 'calle',
                    'suburb' => 'colonia',
                    'idUser' => 'id del usuario',

                    'clientInvoicing.businessName' => 'razón social',
                    'clientInvoicing.rfc' => 'rfc',
                    'clientInvoicing.country' => 'pais de facturacion',
                    'clientInvoicing.state' => 'estado de facturacion',
                    'clientInvoicing.city '=> 'ciudad de facturacion',
                    'clientInvoicing.postalCode' => 'codigo postal de facturacion',
                    'clientInvoicing.numberInterior' => 'numero interior de facturacion',
                    'clientInvoicing.numberOutdoor' => 'numero exterior de facturacion',
                    'clientInvoicing.street' => 'calle de facturacion',
                    'clientInvoicing.suburb' => 'colonia de facturacion',

                    'plaque' => 'placas',
                    'model' => 'modelo',
                    'brand' => 'marca',
                    'serie' => 'serie',
                    'color' => 'color',

                    'clientInstallation.country' => 'pais de instalación',
                    'clientInstallation.state' => 'estado de instalación',
                    'clientInstallation.city '=> 'ciudad de instalación',
                    'clientInstallation.postalCode' => 'codigo postal de instalación',
                    'clientInstallation.numberInterior' => 'numero interior de instalación',
                    'clientInstallation.numberOutdoor' => 'numero exterior de instalación',
                    'clientInstallation.street' => 'calle de instalación',
                    'clientInstallation.suburb' => 'colonia de instalación',

                    'gpsEquipment.*.idEquipment' => 'id del equipo',
                    'gpsEquipment.*.quantity' => 'cantidad de equipos',

                    'idExecutive' => 'id de la ejecutiva',
                    'period' => 'periodo',
                    'date' => 'fecha'
                ];
                break;
        }
        return $attributes;
    }

    public function response(array $errors)
    {
        return response()->json($errors, 422);
    }
}
<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class RoutesRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //obtener el URI desde donde se esta instanciando el objeto y dividirlo en palabras individuales en cada /
        $method = explode('/',$this::path());
        
        //selecionar el ultimo elemento de la ruta 'unitOperator/store' y crear la regla dependiendo el caso
        switch($method[count($method)-1]){
            case 'storeRoute':
                $rules = [
                    'name' => 'required',
                    'idUnit' => 'required|integer',
                    'idOperator' => 'required|integer',
                    'characteristic' => 'required'
                ];
                break;
            case 'updateRoute':
                $rules = [
                    'idRoute' => 'required|integer',
                    'name' => 'required',
                    'idUnit' => 'required|integer',
                    'idOperator' => 'required|integer',
                    'characteristic' => 'required'
                ];
                break;
            case 'remRoute':
                $rules = [
                    'idRoute' => 'required|integer',
                ];
                break;
        }
        return $rules;
    }

    public function attributes()
    {
        $method = explode('/',$this::path());
        switch($method[count($method)-1]){
            case 'storeRoute':
                $attributes = [
                    'name' => 'nombre de la ruta',
                    'idUnit' => 'id de la unidad',
                    'idOperator' => 'id del operador',
                    'characteristic' => 'caracteristica de la ruta'
                ];
                break;
            case 'updateRoute':
                $attributes = [
                    'idRoute' => 'id de la ruta',
                    'name' => 'nombre de la ruta',
                    'idUnit' => 'id de la unidad',
                    'idOperator' => 'id del operador',
                    'characteristic' => 'caracteristica de la ruta'
                ];
                break;
            case 'remRoute':
                $attributes = [
                    'idRoute' => 'id de la ruta'
                ];
                break;
        }
        return $attributes;
    }

    public function response(array $errors)
    {
        return response()->json($errors, 422);
    }
}
<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class TypeVehiclesRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //obtener el URI desde donde se esta instanciando el objeto y dividirlo en palabras individuales en cada /
        $method = explode('/',$this::path());
        
        //selecionar el ultimo elemento de la ruta 'unitOperator/store' y crear la regla dependiendo el caso
        switch($method[count($method)-1]){
            case 'store':
                $rules = [
                    'name' => 'required'
                ];
                break;
            case 'update':
                $rules = [
                    'idTypeVehicle' => 'required|integer',
                    'name' => 'required'
                ];
                break;
            case 'rem':
                $rules = [
                    'idTypeVehicle' => 'required|integer'
                ];
                break;
        }
        return $rules;
    }

    public function attributes()
    {
        $method = explode('/',$this::path());
        switch($method[count($method)-1]){
            case 'store':
                $attributes = [
                    'name' => 'nombre o descripcion'
                ];
                break;
            case 'update':
                $attributes = [
                    'idTypeVehicle' => 'id del tipo de vehiculo',
                    'name' => 'nombre o descripcion'
                ];
                break;
            case 'rem':
                $attributes = [
                    'idTypeVehicle' => 'id del tipo de vehiculo'
                ];
                break;
        }
        return $attributes;
    }

    public function response(array $errors)
    {
        return response()->json($errors, 422);
    }
}
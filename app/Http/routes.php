<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods:  POST, GET, OPTIONS, PUT, DELETE');
header('Access-Control-Allow-Headers:  Content-Type, X-Auth-Token, Origin, Authorization, x-token');

Route::get('/', function () {
    return view('welcome');
});

//Auth
Route::group(['prefix' => 'auth'], function () {
    Route::post('/login', 'LogInController@logIn');
    Route::post('/setToken', 'LogInController@setToken');
    Route::post('/getToken', 'LogInController@getToken');
    Route::post('/remToken', 'LogInController@remToken');
});

Route::group(['prefix' => 'profile'], function () {
    Route::get('/getTheme/{idProfile}', 'UserController@getTheme');
    Route::post('/setTheme', 'UserController@setTheme');
});

//Config
Route::group(['prefix' => 'routes'], function () {
    Route::post('/storeRoute', 'RoutesController@storeRoute');
    Route::get('/getRoute', 'RoutesController@getRoute');
    Route::post('/remRoute', 'RoutesController@remRoute');
    Route::post('/updateRoute', 'RoutesController@updateRoute');
});

Route::group(['prefix' => 'typeVehicles'], function () {
    Route::post('/store', 'TypeVehiclesController@store');
    Route::get('/get', 'TypeVehiclesController@get');
    Route::post('/rem', 'TypeVehiclesController@rem');
    Route::post('/update', 'TypeVehiclesController@update');
});

Route::group(['prefix' => 'typeStatusOrders'], function () {
    Route::post('/store', 'TypeStatusOrdersController@store');
    Route::get('/get', 'TypeStatusOrdersController@get');
    Route::post('/rem', 'TypeStatusOrdersController@rem');
    Route::post('/update', 'TypeStatusOrdersController@update');
});

Route::group(['prefix' => 'units'], function () {
    Route::post('/store', 'UnitsController@store');
    Route::get('/get', 'UnitsController@get');
    Route::post('/rem', 'UnitsController@rem');
    Route::post('/update', 'UnitsController@update');
});

Route::group(['prefix' => 'operators'], function () {
    Route::post('/store', 'OperatorsController@store');
    Route::get('/get', 'OperatorsController@get');
    Route::post('/rem', 'OperatorsController@rem');
    Route::post('/update', 'OperatorsController@update');
    Route::post('/updateImage', 'OperatorsController@updateImage');
});

Route::group(['prefix' => 'unitsOperators'], function () {
    Route::post('/store', 'UnitsOperatorsController@store');
    Route::get('/get/{id_business}', 'UnitsOperatorsController@get');
    Route::post('/rem', 'UnitsOperatorsController@rem');
    Route::post('/update', 'UnitsOperatorsController@update');
});

Route::group(['prefix' => 'products'], function () {
    Route::get('/getPackages', 'ProductsController@getPackages');
    Route::get('/get/{id_business}', 'ProductsController@getProducts');
});

Route::group(['prefix' => 'executives'], function () {
    Route::get('/get/{id_user}', 'ExecutivesController@get');
});

Route::group(['prefix' => 'client'], function () {
    Route::post('/get', 'ClientController@index');
    Route::get('/show/{phone}', 'ClientController@show');
    Route::post('/updateDir', 'ClientController@updateDir'); 
});

Route::group(['prefix' => 'csc'], function () {
    Route::post('/get', 'CSCController@index');
});

Route::group(['prefix' => 'departments'], function () {
    Route::post('/store', 'DepartmentsController@store');
    Route::post('/update', 'DepartmentsController@update');
    Route::post('/delete', 'DepartmentsController@delete');
    Route::get('/get', 'DepartmentsController@get');
});

Route::group(['prefix' => 'sections'], function () {
    Route::post('/store', 'SectionsController@store');
    Route::post('/update', 'SectionsController@update');
    Route::post('/delete', 'SectionsController@delete');
    Route::get('/get', 'SectionsController@get');
});

//process
Route::group(['prefix' => 'sms'], function () {
    Route::post('/store', 'SmsSenderController@store');
});

Route::group(['prefix' => 'indicators'], function () {
    Route::post('/get', 'IndicatorsController@get');
    Route::post('/store', 'IndicatorsController@store');
    Route::post('/update', 'IndicatorsController@update');
    Route::post('/updateRaiting', 'IndicatorsController@updateRaiting');
    Route::post('/delete', 'IndicatorsController@delete');
});

Route::group(['prefix' => 'whatsapp'], function () {
    Route::get('/get', 'SendWhatsapp@get');
    Route::post('/store', 'SendWhatsapp@store');
    Route::post('/update', 'SendWhatsapp@update');
});

Route::group(['prefix' => 'tickets'], function () {
    Route::get('/get', 'TicketsController@get');
    Route::post('/getTickets', 'TicketsController@getTickets');
    Route::post('/getIndicators', 'TicketsController@getIndicators');
    Route::post('/store', 'TicketsController@store');
    Route::post('/answer', 'TicketsController@answer');
    Route::post('/postpone', 'TicketsController@postpone');
    Route::post('/close', 'TicketsController@close');
});

Route::group(['prefix' => 'intranet'], function () {
    Route::get('/getList', 'IntranetController@getList');
    Route::get('/get/{id_user}', 'IntranetController@get');
    Route::post('/store', 'IntranetController@store');
    Route::post('/update', 'IntranetController@update');
    Route::post('/update', 'IntranetController@update');
    Route::post('/delete', 'IntranetController@delete');
});

Route::group(['prefix' => 'ordersLM'], function () {
    Route::post('/store', 'OrdersLMController@store');
    Route::post('/get','OrdersLMController@get');
    Route::post('/getOrdersAsignment','OrdersLMController@getOrdersAsignment');
    Route::post('/saveUrl','OrdersLMController@saveUrl');
});

Route::group(['prefix' => 'ordersClients'], function () {
    Route::post('/store', 'OrdersClientsController@store');
    Route::post('/get/{id_permission}', 'OrdersClientsController@get');
    Route::post('/getDetails', 'OrdersClientsController@getDetails');
    Route::post('/saveVoucher', 'OrdersClientsController@saveVoucher');
    Route::post('/saveVoucherUrl', 'OrdersClientsController@saveVoucherUrl');
    Route::post('/saveStatusOrder', 'OrdersClientsController@saveStatusOrder');
    Route::post('/saveGuide', 'OrdersClientsController@saveGuide');
    Route::post('/saveGuideUrl', 'OrdersClientsController@saveGuideUrl');
});

Route::group(['prefix' => 'scheduleInstallation'], function () {
    Route::post('/store', 'OrdersClientsEscheduleInstallationController@store');
    Route::post('/createNewClient', 'OrdersClientsEscheduleInstallationController@createNewClient');
    Route::post('/get/{id_permission}', 'OrdersClientsEscheduleInstallationController@get');
    Route::post('/getDetails', 'OrdersClientsEscheduleInstallationController@getDetails');
    Route::post('/saveVoucher', 'OrdersClientsEscheduleInstallationController@saveVoucher');
    Route::post('/saveVoucherUrl', 'OrdersClientsEscheduleInstallationController@saveVoucherUrl');
    Route::post('/saveStatusOrder', 'OrdersClientsEscheduleInstallationController@saveStatusOrder');
    Route::post('/getClient', 'OrdersClientsEscheduleInstallationController@getClient'); 
    Route::get('/getClients', 'OrdersClientsEscheduleInstallationController@getClients'); //todos los clientes
    Route::post('/updateClient','OrdersClientsEscheduleInstallationController@updateClient'); //ruta para update clients
    Route::post('/rem','OrdersClientsEscheduleInstallationController@rem'); //cambiar status
    Route::get('/getUpPayments/{id_user}', 'OrdersClientsEscheduleInstallationController@getUpPayments');


});

Route::group(['prefix' => 'stockInventory'], function () {
    Route::post('/store', 'OrdersClientsStockInventoryController@store');
    Route::post('/saveStatusOrder', 'OrdersClientsStockInventoryController@saveStatusOrder');
    Route::post('/saveVoucher', 'OrdersClientsStockInventoryController@saveVoucher');
    Route::post('/saveVoucherUrl', 'OrdersClientsStockInventoryController@saveVoucherUrl');
    Route::post('/saveGuide', 'OrdersClientsStockInventoryController@saveGuide');
    Route::post('/saveGuideUrl', 'OrdersClientsStockInventoryController@saveGuideUrl');
    Route::post('/get/{id_permission}', 'OrdersClientsStockInventoryController@get');
    Route::post('/getDetails', 'OrdersClientsStockInventoryController@getDetails');
    Route::get('/getRequests/{id_asesor}', 'OrdersClientsStockInventoryController@getRequests');
    Route::post('/reject', 'OrdersClientsStockInventoryController@reject');


});

//I don't know
Route::group(['prefix' => 'user'], function () {
    Route::post('/login', 'UserController@get_login');
});

Route::group(['prefix' => 'logs'], function () {
    Route::post('/save/{evento}/{id_usuario}/{tabla}/{id_reg}/{values}', 'LogController@store');
    Route::get('/get', 'LogController@index');
});

Route::post('/creditocobranza', 'CCController@index');

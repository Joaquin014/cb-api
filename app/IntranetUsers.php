<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IntranetUsers extends Model
{
    //
    protected $table = 'intranet_users';
}

<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class StockInventoryDetails extends Model {
    
    protected $table = 'stock_inventory_details';
}

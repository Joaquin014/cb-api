<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeVehicles extends Model
{
    protected $table = 'type_vehicles';
}

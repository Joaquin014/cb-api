<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Executive extends Model
{
    //
    protected $table = 'config_executives';
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IndicatorsProspects extends Model
{
    protected $table = 'indicators_prospects';
}

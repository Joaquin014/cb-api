<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientQuotation extends Model
{
    protected $table = 'clients_quotation';
}

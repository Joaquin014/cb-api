<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CreditCollection extends Model
{
    protected $table = 'credit_collection';
}

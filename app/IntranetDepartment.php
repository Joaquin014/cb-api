<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IntranetDepartment extends Model
{
    //
    protected $table = 'intranet_department';
}
